// import config from './common/config';

export default {
  pages: [
    // 'pages/orderDetail/signFor',
    'pages/loading/index',

    'pages/reporting/index',

    'pages/gateManage/index',
    'pages/gateManage/search',

    'pages/gateManage/salesData/detail',
    'pages/gateManage/salesData/history',
    'pages/gateManage/salesReport/detail',
    'pages/gateManage/salesReport/sales',

    'pages/realNameAuthentication/index',
    'pages/realNameAuthentication/detail',
    'pages/receivingGate/index',
    'pages/receivingGate/edit',
    'pages/consignmentOrder/index',
    'pages/consignmentOrder/success',
    'pages/indexDetail/index',
    'pages/my/index',
    'pages/login/index',
    'pages/index/index',
    'pages/index/searchPage',
    'pages/order/index',
    'pages/orderDetail/index',
    'pages/orderDetail/refuse',
    'pages/orderDetail/signFor',


    // demo
    'components/form/demo/demo1',
    'components/searchPage/demo/demo1',
  ],
  window: {
    // backgroundTextStyle: 'black',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black',
  },
  tabBar: {
    color: '#666',
    borderStyle: 'black',
    backgroundColor: '#fff',
    selectedColor: '#44b549',
    list: [
      {
        pagePath: 'pages/index/index',
        text: '找货',
        iconPath: 'assets/index.png',
        selectedIconPath: 'assets/index_active.png',
      },
      {
        pagePath: 'pages/reporting/index',
        text: '上报',
        iconPath: 'assets/normal.png',
        selectedIconPath: 'assets/normal_active.png',
      },
      {
        pagePath: 'pages/gateManage/index',
        text: '档口',
        iconPath: 'assets/stall.png',
        selectedIconPath: 'assets/stall_active.png',
      },
      {
        pagePath: 'pages/my/index',
        text: '我的',
        iconPath: 'assets/my.png',
        selectedIconPath: 'assets/my_active.png',
      },
    ],
  },
};
