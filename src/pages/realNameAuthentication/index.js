import React, { useState, useRef, useEffect, useCallback } from 'react';
// import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import request from '../../common/request';
import PageContainer from '../../components/pageContainer';
import VerticalSeparation from '../../components/VerticalSeparation';
import FormList from '../../components/formList';
import PagefooterButton from '../../components/pagefooterButton';
import request from '../../common/request';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import './index.scss'
import { saveToken } from '../../common/wx';
// name
// stallName
// const initValues = {
//   phoneNumber: 15554723296,
//   userName: '批发市场档主',
// };
// const marketIdList = [{id:1,name: '市场1'},{id:2,name: '市场2'}];
const unitTypeList = [
  {id:1,name: '企业制'},
  {id:2,name: '合伙制'},
  {id:2,name: '家族制'},
  {id:2,name: '夫妻档'},
  {id:2,name: '业主+业务员'},
];
function Index() {
  const formRef = useRef(null);
  const [initValues, setInitValues] = useState({});// marketIdList
  const [marketIdList, setMarketIdList] = useState({});

  // 查询市场
  const getMarketIdList = useCallback((arr)=>{
    request({method : 'GET', tartget: 'baseMarketList', data: { condition: { marketRegion: arr[0], marketCity: arr[1]/*, marketArea: arr[2] */ } } }).then((data)=>{
      if (data.success) {
        setMarketIdList(data.models);
      }
    });
  },[]);

  useEffect(()=>{
    request({method : 'GET', tartget: 'userInfoQuery', data: {} }).then((data)=>{
      if (data.success) {
        const city = [
          data.model.marketRegion,
          data.model.marketCity,
          // data.model.marketArea,
        ];
        getMarketIdList(city);
        setInitValues({...data.model, city });
      }
    });
  },[getMarketIdList]);

  const regionSelectOnChange = useCallback((e)=>{
    // 清空市场
    formRef.current.setFieldsValue({
      marketId: undefined,
    });
    // 重新查询市场
    getMarketIdList(e);
  },[getMarketIdList]);

  return (
    <PageContainer noPadding>
      <VerticalSeparation />
      <FormList formRef={formRef} initValues={initValues}>
        <FormList.InputItem type='inputText' label='手机号码' name='phoneNumber' disabled />
        <FormList.ItemText label='用户身份' value='批发市场档主' />
        <FormList.InputItem type='inputText' label='档主行名' name='stallName' require />
        <FormList.RegionSelectItem onChange={regionSelectOnChange} label='目的城市' name='city' require />
        <FormList.InputItem
          range={marketIdList}
          type='inputSelect'
          label='所属市场'
          name='marketId'
          rangeKey='marketName'
          require
        />
        <FormList.InputItem type='inputText' label='档口编号' name='stallCode' require />
        <FormList.InputItem type='inputText' label='档主姓名' name='name' require />
        <FormList.InputItem
          range={unitTypeList}
          type='inputSelect'
          label='组织形式'
          name='unitType'
        />
        <FormList.FileItem label='请上传档主照片' name='selfieFileList' require describe='请上传档主的个人照片，或者与市场助手的合影' />
        <FormList.FileItem label='请上传档口正门口照片' name='identifyFileList' require describe='请拍照上传档口的正门口照片，注意要能显示档口牌匾' />
        <FormList.FileItem label='请上传身份证正反面照片' name='gateFileList' multipleLength={2} require />
      </FormList>
      <VerticalSeparation height={58} />
      <PagefooterButton
        oneButton={{
          text:'提交实名认证',
          onClick:()=>{
            formRef.current.validateFields((values,error)=>{
              if (!error) {
                if (!values.selfieFileList || values.selfieFileList.length === 0 ) {
                  Taro.showToast({ title: '请上传档主照片', icon: 'none' });
                  return;
                }
                if (!values.identifyFileList || values.identifyFileList.length === 0 ) {
                  Taro.showToast({ title: '请上传档口正门口照片', icon: 'none' });
                  return;
                }
                if (!values.gateFileList || values.gateFileList.length === 0 ) {
                  Taro.showToast({ title: '请上传身份证正反面照片', icon: 'none' });
                  return;
                }
                if (values.gateFileList.length !== 2 ) {
                  Taro.showToast({ title: '请上传2张身份证照片', icon: 'none' });
                  return;
                }
                request({method : 'POST', tartget: 'userInfoAuthenticate', data: values }).then((data)=>{
                  if (data.success) {
                    Taro.showToast({
                      title: '提交成功',
                      icon: 'none',
                    });
                    const user = Taro.getStorageSync('user') || {};
                    saveToken({
                      ...user,
                      certifiedStatus: data.model.certifiedStatus
                    });
                    setTimeout(()=>{
                      //到首页
                      Taro.reLaunch({ url: '/pages/index/index' });
                    },2)
                  }
                });
              }
            })
          },
        }}
      />
    </PageContainer>
  )
}

export default Index;
