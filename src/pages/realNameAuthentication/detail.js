import React, { useState, useEffect } from 'react';
import { Image, View } from '@tarojs/components';
// import request from '../../common/request';
import PageContainer from '../../components/pageContainer';
import VerticalSeparation from '../../components/VerticalSeparation';
import FormList from '../../components/formList';
import request from '../../common/request';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import './detail.scss'
// import loginButtonPng from '../../assets/loginButton.png';
import SetAvatar from '../../components/setAvatar';
// name
// stallName

// const initValues = {
//   phoneNumber: 15554723296,
//   userName: '批发市场档主',
// };
const unitTypeList = [
  {id:1,name: '企业制'},
  {id:2,name: '合伙制'},
  {id:2,name: '家族制'},
  {id:2,name: '夫妻档'},
  {id:2,name: '业主+业务员'},
];
function Index() {
  const [initValues, setInitValues] = useState({});
  // const [marketIdList, setMarketIdList] = useState({});

  useEffect(()=>{
    request({method : 'GET', tartget: 'userInfoQuery', data: {} }).then((data)=>{
      if (data.success) {
        setInitValues(data.model);
      }
    });
    // request({method : 'GET', tartget: 'baseMarketList', data: {} }).then((data)=>{
    //   if (data.success) {
    //     setMarketIdList(data.models);
    //   }
    // });
  },[]);

  return (
    <View>
      <PageContainer noPadding>
        <VerticalSeparation />
        <View className='topHead'>
          <View className='topHeadText'>个人头像</View>
          <View>
            <SetAvatar urlImg={initValues.avatar}>
              <Image
                className='topHeadImg'
              />
            </SetAvatar>
          </View>
        </View>
        <VerticalSeparation />
        <FormList detail initValues={initValues}>
          <FormList.ItemText
            label='认证状态'
            valueRender={
              <View className='yrz'>已认证</View>
            }
          />
          <FormList.InputItem type='inputText' label='手机号码' name='phoneNumber' />
          <FormList.ItemText label='用户身份' value='批发市场档主' />
          <FormList.InputItem type='inputText' label='档主行名' name='stallName' require />
          <FormList.ItemText label='所属市场' value={initValues.marketName} />
          <FormList.InputItem type='inputText' label='档口编号' name='stallCode' require />
          <FormList.InputItem type='inputText' label='档主姓名' name='name' require />
          <FormList.InputItem
            range={unitTypeList}
            type='inputSelect'
            label='组织形式'
            name='unitType'
          />
          <FormList.FileItem label='档主照片' name='selfieFileList' />
          <FormList.FileItem label='档口正门口照片' name='identifyFileList' />
          <FormList.FileItem label='身份证正反面照片' name='gateFileList' />
        </FormList>
      </PageContainer>
    </View>
  )
}

export default Index;
