import React, { useEffect, useCallback } from 'react';
import Taro, { usePullDownRefresh, } from '@tarojs/taro';
import { View, Text,  SwiperItem, Swiper,  Picker } from '@tarojs/components';
import useFetchRequest from '../../common/useFetchRequest';
import {preRequest} from '../../utils/index'
import SalesDataList from './salesData/index';
import SalesReportList from './salesReport/index';
import SalesGoodsList from './salesGoods/index';
import './index.scss';

export default function GateManageIndex(props) {
  const { state, setAction, request,  setUpdate } = useFetchRequest();
  const { instants = {}, instantList = [] } = state;
  // 获取档口信息
  const getInstantInfo = useCallback(params => {
    request({
      url: 'saleInstantQuery',
      data: params,
      callback: res => {
        Taro.stopPullDownRefresh();
        res.success && setAction({ instants: res.model });
        setUpdate();
      },
    });
     // eslint-disable-next-line
  }, []);

  // 获取用户档口
  const getUserStallList = useCallback(() => {
    request({
      url: 'userStallList',
      callback: res => {
        if (res.success && res.models) {
          const _instantList = res.models.map(item => ({
            ...item,
            name: `${item.marketName} | ${item.stallCode}`,
          }));

          const alive = _instantList.find(i=>i.id === state.instantId )
          let instantId =res.models[0].id
          if(alive){
            instantId = state.instantId
          }
          setAction({ instantList: _instantList, instantId });
          getInstantInfo({ condition: { stallId: instantId } });
        }
      },
    });
    // eslint-disable-next-line
  }, []);


  // 初始化
  useEffect(() => {
    setAction({ types: 0, index: 0 });
    preRequest(getUserStallList)
     // eslint-disable-next-line
  }, []);


 // 切换swiper
  const swiperOnChange = useCallback(e => {
    const index = e.detail.current;
    if (index !== state.index) {
      setUpdate(setAction({ index }));
    }
     // eslint-disable-next-line
  }, []);

  // 切换swiper
  const navOnChange = useCallback(e => {
    const index = e.currentTarget.dataset.index;
    if (index !== state.index) {
      setUpdate(setAction({ index }));
    }
    // eslint-disable-next-line
  }, []);


  // 切换档口
  const instantOnChange = useCallback(e => {
    const index = e.detail.value;
    const _instantList = state.instantList;
    const instantId = _instantList[index].id;
    if (instantId !== state.instantId) {
      setAction({ instantId });
      getInstantInfo({ condition: { stallId: instantId } });
    }
    // eslint-disable-next-line
  }, []);


  // 下拉刷新
  usePullDownRefresh(() => {
    getUserStallList();
  }, []);

  return (
    <View className='sales-page'>
      {/* 档口 数据统计 */}
      <View className='sales-total'>
        <View className='company'>
          <View>
            <Text className='ft-34'>{instants.stallName}</Text>
            <View className='addr ft-26'>{instants.marketName} | {instants.stallCode}</View>
          </View>
          <View className='instants'>
            <Picker rangeKey='name' className='picker' range={instantList} onChange={instantOnChange} value={instantList.findIndex(i=>state.instantId === i.id)}>
              <Text>切换档口</Text>
            </Picker>
          </View>
        </View>

        {/* 销售 数据统计 */}
        <View className='sales-info'>
          <View className='info-item'>
            <Text className='ft-34'>{((instants.stallOrderQuantity - instants.totalSaleQuantity) || 0).toFixed(2)}</Text>
            <View className='label ft-26'>剩余待销售</View>
          </View>
          <View className='info-item'>
            <Text className='ft-34'>{(instants.expectSaleQuantity||0).toFixed(2)} </Text>
            <View className='label ft-26'>将到货代卖</View>
          </View>
          <View className='info-item'>
            <Text className='ft-34'>{(instants.daySaleQuantity||0).toFixed(2)} </Text>
            <View className='label ft-26'>昨日共卖出</View>
          </View>
        </View>
      </View>

      {/* 切换 tab */}
      <View className='sales-header ft-28'>
        <View className='header-nav' data-index={0} onClick={navOnChange}>
          <Text className={state.index == 0 && 'active'}>实时销售数据</Text>
        </View>
        <View className='header-nav' data-index={1} onClick={navOnChange}>
          <Text className={state.index == 1 && 'active'}>销售报表</Text>
        </View>
        <View className='header-nav' data-index={2} onClick={navOnChange}>
          <Text className={state.index == 2 && 'active'}>在售货品</Text>
        </View>
      </View>

      {/* swiper 切换 tab */}
      <Swiper className='sales-box' circular current={state.index} onChange={swiperOnChange}>
        <SwiperItem className='sales-info'>
          <SalesDataList stallId={state.instantId} tid={props.tid} />

          <View className='empty-box'></View>
        </SwiperItem>

        <SwiperItem className='sales-info'>
          <SalesReportList stallId={state.instantId} tid={props.tid} />
          <View className='empty-box'></View>
        </SwiperItem>

        <SwiperItem className='sales-info'>
          <SalesGoodsList stallId={state.instantId} tid={props.tid} />
          <View className='empty-box'></View>
        </SwiperItem>
      </Swiper>
    </View>
  );
}
