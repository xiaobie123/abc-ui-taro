import React, { useEffect, useCallback } from 'react';
import Taro from '@tarojs/taro';
import { View, Image, Text, Navigator, Block } from '@tarojs/components';
import useFetchRequest from '../../../common/useFetchRequest';
import './index.scss';

export default function SalesGoods(props) {
  const FetchRequest = useFetchRequest();
  const { state, setAction, request, setUpdate } = FetchRequest
  const { viewImageFiles, dateFormat, splitImageFile } = FetchRequest
  const { stallId } = props;
  const { salesList = [] } = state;

  const getReportingList = useCallback(params => {
    request({
      url: 'reportingSaleProductList',
      data: params,
      callback: res => {
        res.success && setUpdate(
          setAction({ salesList: res.models })
        );
      },
    });
  }, [request, setAction, setUpdate]);

  useEffect(() => {
    if(stallId){
      getReportingList({ condition: { stallId } });
    }
    // eslint-disable-next-line
  }, [stallId]);


  return (
    <Block>
      <Navigator hover-class='none' url={`/pages/gateManage/search?keyword=productName&page=salesGoods&stallId=${stallId}`}>
        <View className='search-box'>请输入货品名称</View>
      </Navigator>

      {
        salesList && salesList.map((goods,index) => (
        <View key={index} onClick={()=>{
            Taro.navigateTo({url:'/pages/gateManage/salesData/history?stallOrderId='+ goods.stallOrderId})
          }}
        >
          <View className='product-card'>
            <View className='product-goods'>
              <View className='product-info'>
                <Image
                  className='goods-img icon-img-bg'
                  src={splitImageFile({fileId:goods.productCoverId,fileCode:goods.productCoverCode })}
                  onClick={viewImageFiles.bind(null,splitImageFile({fileId:goods.productCoverId,fileCode:goods.productCoverCode }))}
                />
                <View className='goods-info'>
                  <View className='info'>{ goods.productName}</View>
                  <View className='info'>订单编号: {goods.stallOrderNo}</View>
                  <View className='info'>{dateFormat('yyyy-MM-dd', goods.arriveTime)} 到货</View>
                </View>
              </View>

              <View className='goods-spec ft-26'>
                {
                  goods.saleRecordItemList && goods.saleRecordItemList.map((item,i)=>(
                    <View className='spec-item' key={i}>
                      <Text className='label'>{item.productSpec}</Text>
                      <Text className='value'>{((item.reservedQuantity - item.saleQuantity)||0).toFixed(2)} 件待销售</Text>
                    </View>
                  ))
                }
              </View>
            </View>
          </View>
        </View>
      ))}
    </Block>
  );
}
