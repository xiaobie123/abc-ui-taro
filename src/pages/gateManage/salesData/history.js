import React, {  useEffect, useCallback } from 'react';
import Taro, {usePullDownRefresh}  from '@tarojs/taro';
import { View, Text, Image  } from '@tarojs/components';
import useFetchRequest from '../../../common/useFetchRequest';
import './history.scss';




export default function SalesHistoryDetail(props) {
  const FetchRequest = useFetchRequest();
  const { state, setAction, request } = FetchRequest;
  const { dateFormat,  splitImageFile, setUpdate, viewImageFiles, getNavigatorExtra } = FetchRequest;
  const { history = {}, historyList = [] } = state;


  const changeList = useCallback((name,result)=>{
    if(result && result.success ){
      Taro.stopPullDownRefresh();
      setAction({ [name]: result.model });
      setUpdate();
    }
  },[setAction, setUpdate])

  // 获取数据
  const getHistoryList = useCallback(params => {
    request({
      url: 'saleHistoryQuery',
      data: params,
      callback: changeList.bind(null,'history')
    });
    request({
      url: 'saleHistoryItemList',
      data: params,
      callback: changeList.bind(null,'historyList')
    });
  }, [changeList, request]);




  useEffect(() => {
    const params = getNavigatorExtra(props.tid);
    getHistoryList({ condition: params });
    // eslint-disable-next-line
  }, []);

  usePullDownRefresh(() => {
    const params = getNavigatorExtra(props.tid);
    getHistoryList({ condition: params });
  });

  return (
    <View className='history-page'>
      <View className='card-goods'>
        <Image
          className='goods-img icon-img-bg'
          src={splitImageFile({fileId:history.productCoverId,fileCode:history.productCoverCode})}
          onClick={viewImageFiles.bind(null,splitImageFile({fileId:history.productCoverId,fileCode:history.productCoverCode}))}
        />
        <View className='goods-info'>
          <View className='info'>订单编号: {history.stallOrderNo}</View>
          <View className='info'>代卖编号: {history.consignorOrderNo}</View>
          <View className='info'>货品名称: {history.productName}</View>
        </View>
      </View>

      <View className='sales-total'>
        <View className='total-item'>
          <View className='value'>￥{(history.totalSaleAmount||0).toFixed(2)}</View>
          <View className='txt'>总销售金额</View>
        </View>
        <View className='total-item'>
          <View className='value'>{(history.totalSaleQuantity||0).toFixed(2)}</View>
          <View className='txt'>总销售件数</View>
        </View>
        <View className='total-item'>
          <View className='value'>{((history.stallOrderQuantity - history.totalSaleQuantity) || 0).toFixed(2)}</View>
          <View className='txt'>总待销售件数</View>
        </View>
      </View>

      <View className='history-list ft-26'>
        {
          historyList && historyList.map(i => (
            <View className='history-item' key={i.saleTime}>
              <View className='history-title'>
                <Text className='title'>{dateFormat('yyyy-MM-dd', i.saleTime)}</Text>
              </View>
              <View className='history-card'>
                <View className='item'>
                  <Text>销售件数</Text>
                  <Text>{(i.totalSaleQuantity || 0).toFixed(2)}件</Text>
                </View>
                <View className='item'>
                  <Text>销售重量</Text>
                  <Text>{(i.totalSaleWeight ||0).toFixed(2)}斤</Text>
                </View>
                <View className='item'>
                  <Text>销售金额</Text>
                  <Text>{(i.totalSaleAmount ||0).toFixed(2)}元</Text>
                </View>
              </View>
            </View>
          ))
        }
      </View>
    </View>
  );
}
