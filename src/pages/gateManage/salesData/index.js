import React, {  useEffect, useCallback } from 'react';
import Taro from '@tarojs/taro';
import { View, Text, Image,  Navigator } from '@tarojs/components';
import useFetchRequest from '../../../common/useFetchRequest';


export default function SalesDataList(props) {
  const FetchRequest = useFetchRequest();
  const {state, setAction, request } = FetchRequest
  const {setUpdate, splitImageFile,  viewImageFiles,  dateFormat  } = FetchRequest
  const { salesList = [] } = state;
  const { stallId, tid } = props;

  const getInstantList = (res)=>{
    const timeList = [];
    if(Array.isArray(res.models)){
      res.models.forEach(i => {
        const isAlive = timeList.find(t => t.time === i.saleTime);
        if (isAlive) {
          isAlive.list.push(i);
          isAlive.amount += i.totalSaleAmount;
          isAlive.quantity += i.totalSaleQuantity;
        } else {
          timeList.push({
            time: i.saleTime,
            list: [i],
            amount: i.totalSaleAmount,
            quantity: i.totalSaleQuantity,
          });
        }
      })
    }
    return timeList
  }

  // 获取列表数据
  const getInstantItemList = useCallback(params => {
    request({
      url: 'saleInstantItemList',
      data: params,
      callback: res => {
        const timeList = getInstantList(res)
        res.success && setAction({ salesList: timeList });
        setUpdate();
      },
    });
    // eslint-disable-next-line
  }, []);

  // 初始化
  useEffect(() => {
    setAction({ types: 0, index: 0 })
    setUpdate();
    // eslint-disable-next-line
  }, []);

  // stallId change
  useEffect(() => {
    if(stallId){
      getInstantItemList({ condition: { stallId } });
    }
    // eslint-disable-next-line
  }, [stallId,tid]);


  return (
    <React.Fragment>
      <Navigator hover-class='none' url={`/pages/gateManage/search?keyword=productName&page=salesData&stallId=${stallId}`}>
        <View className='search-box'>请输入货品名称</View>
      </Navigator>
      {
        salesList.map((goods,index) => (
          <View className='card-bar' key={index}>
            <View className='card-title'>
              <Text className='time'>{dateFormat('yyyy-MM-dd', goods.time)}</Text>
              <Text className='total'>
                合计:<Text className='value'>{(goods.quantity || 0 ).toFixed(2)}件</Text>
                收入:<Text className='value'>￥{(goods.amount ||0 ).toFixed(2)}</Text>
              </Text>
            </View>
            {
              goods.list && goods.list.map((item,i) => (
                <View key={i} onClick={()=>{
                    Taro.navigateTo({url:'/pages/gateManage/salesData/detail?saleRecordId='+item.id})
                  }}
                >
                  <View className='card-goods'>
                    <Image
                      className='goods-img icon-img-bg'
                      src={splitImageFile({fileId:item.productCoverId,fileCode:item.productCoverCode})}
                      onClick={viewImageFiles.bind(null,splitImageFile({fileId:item.productCoverId,fileCode:item.productCoverCode}))}
                    />
                    <View className='goods-info'>
                      <View className='title ft-30'>{ item.productName}</View>
                      <View className='info'>订单编号: {item.stallOrderNo}</View>
                      <View className='info'>上报时间: {dateFormat('yyyy-MM-dd hh:mm',item.saleRecordTime)}</View>
                    </View>
                    <View className='total'>
                      <View className='quantity ft-26'>x{(item.totalSaleQuantity||0).toFixed(2)}</View>
                      <View className='price ft-28'>￥{(item.totalSaleAmount||0).toFixed(2)}</View>
                    </View>
                  </View>
                </View>
              ))
            }
          </View>
        ))
      }
    </React.Fragment>
  );
}
