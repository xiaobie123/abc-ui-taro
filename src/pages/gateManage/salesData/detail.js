import React, { useEffect, useCallback } from 'react';
import Taro, { usePullDownRefresh } from '@tarojs/taro';
import { View, Text, Image, Navigator  } from '@tarojs/components';
import useFetchRequest from '../../../common/useFetchRequest';
import './detail.scss';



// 销售数据详情

export default function SalesDetail(props) {
  const FetchRequest = useFetchRequest();
  const { request, state, setAction, setUpdate } = FetchRequest;
  const { dateFormat, splitImageFile, getNavigatorExtra, viewImageFiles } = FetchRequest;
  const { record = {} } = state;


  //  获取 数据
  const getReportingList  = useCallback(params => {
    request({
      url: 'reportingSaleRecordQuery',
      data: params,
      callback: res => {
        Taro.stopPullDownRefresh();
        res.success && setAction({ record: res.model });
        setUpdate();
      },
    });
  }, [request, setAction, setUpdate]);



  useEffect(() => {
    const params = getNavigatorExtra(props.tid);
    getReportingList({ condition: params });
     // eslint-disable-next-line
  }, []);

  usePullDownRefresh(() => {
    const params = getNavigatorExtra(props.tid);
    getReportingList({ condition: params });
  });

  return (
    <View className='sales-detail'>


      <View className='sales-total'>
        <View className='total-item'>
          <View className='value'>￥ {(record.totalSaleAmount || 0).toFixed(2)}</View>
          <View className='txt'>销售金额</View>
        </View>
        <View className='total-item'>
          <View className='value'>{(record.totalSaleQuantity ||0).toFixed(2)}</View>
          <View className='txt'>销售件数</View>
        </View>
      </View>

      <Navigator hover-class='none' url={'/pages/gateManage/salesData/history?stallOrderId='+record.stallOrderId}>
        <View className='sales-history ft-28'>
          <Text>查看历史数据</Text>
          <Text className='icon-arrow'></Text>
        </View>
      </Navigator>

      <View className='order-info ft-28'>
        <View className='info-item'>
          <Text>订单编号</Text>
          <Text className='value'>{record.stallOrderNo}</Text>
        </View>
        <View className='info-item'>
          <Text>货品名称</Text>
          <Text className='value'>{record.productName}</Text>
        </View>
        <View className='info-item'>
          <Text>上报时间</Text>
          <Text className='value'>{ dateFormat('yyyy-MM-dd hh:mm:ss', record.saleRecordTime)}</Text>
        </View>

        <View className='upload-group'>
          <View className='upload-list'>
            {
              record.saleRecordFileList && record.saleRecordFileList.map((i,x) => (
                <View className='upload-img-bar' key={x}>
                  <Image
                    mode='aspectFill'
                    className='upload-img'
                    data-index={x}
                    src={splitImageFile(i)}
                    onClick={viewImageFiles.bind(null,record.saleRecordFileList.map(j=>splitImageFile(j)))}
                  />
                </View>
              ))
            }
          </View>
        </View>
      </View>

      <View className='sales-table ft-28'>
        <View className='table-title'>详细销售数据</View>
        <View className='table-bar'>
          <View className='table-body'>
            {
              record.saleRecordItemList && record.saleRecordItemList.map((item,i) => (
                <View className='table-th' key={i}>
                  <View className='table-td ft-28'>
                    <View className='item'>
                      <Text className='label'>产品规格</Text>
                      <Text className='value'>{item.productSpecName}</Text>
                    </View>
                    <View className='item'>
                      <Text className='label'>件数</Text>
                      <Text className='value'>{(item.saleQuantity||0).toFixed(2)} 件</Text>
                    </View>
                    <View className='item'>
                      <Text className='label'>重量</Text>
                      <Text className='value'>{(item.saleWeight||0).toFixed(2)} 斤</Text>
                    </View>
                    <View className='item'>
                      <Text className='label'>销售额</Text>
                      <Text className='value'>{(item.saleAmount||0).toFixed(2)} 元</Text>
                    </View>
                    <View className='item'>
                      <Text className='label'>单价</Text>
                      <Text className='value'>{item.saleQuantity ? (item.saleAmount / item.saleQuantity).toFixed(2) : 0}元/件</Text>
                    </View>
                  </View>
                </View>
              ))
            }
          </View>
        </View>
      </View>

      <View className='empty-box'></View>
      <View style={{ height: '5vh' }}></View>
    </View>
  );
}
