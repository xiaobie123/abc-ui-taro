import React, { useState, useEffect } from 'react';
import { View, Image } from '@tarojs/components';
import Taro, {usePullDownRefresh} from '@tarojs/taro';
import useFetchRequest from '../../../common/useFetchRequest';
import './sales.scss';





export default function SalesReportSales(props) {
  const [reportList, setReportList] = useState([{}]);
  const FetchRequest = useFetchRequest();
  const { request, getNavigatorExtra, dateFormat, splitImageFile,viewImageFiles } = FetchRequest


  const getReportList = ()=>{
    const params = getNavigatorExtra(props.tid);
    request({
      url: 'saleRecordStallList',
      data: { condition: params },
      callback: res => {
        Taro.stopPullDownRefresh()
        res.success && setReportList(res.models);
      },
    });
  }

  useEffect(() => {
    getReportList()
    // eslint-disable-next-line
  }, []);

    // 下拉刷新
    usePullDownRefresh(() => {
      getReportList()
    });

  return (
    <View className='report-sales-page'>
      {
        reportList && reportList.map((item,index) => (
          <View key={index} onClick={()=>{
              Taro.navigateTo({url:'/pages/gateManage/salesData/detail?saleRecordId='+item.id})
            }}
          >
            <View className='card-goods'>
              <Image
                className='goods-img'
                src={splitImageFile({fileId:item.productCoverId,fileCode:item.productCoverCode})}
                onClick={viewImageFiles.bind(null,splitImageFile({fileId:item.productCoverId,fileCode:item.productCoverCode}))}
              />
              <View className='goods-info'>
                <View className='title ft-30'>{item.productName}</View>
                <View className='info'>订单编号: {item.stallOrderNo}</View>
                <View className='info'>上报时间: {dateFormat('yyyy-MM-dd hh:mm:ss', item.saleRecordTime)}</View>
              </View>
              <View className='total'>
                <View className='price ft-28'>￥ {item.totalSaleAmount||0}</View>
                <View className='quantity ft-26'>{item.totalSaleQuantity||0} 件</View>
              </View>
            </View>
          </View>
        ))
      }
    </View>
  );
}
