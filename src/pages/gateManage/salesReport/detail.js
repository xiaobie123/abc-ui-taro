import React, { useState, useEffect  } from 'react';
import { View, Text, Image, Navigator } from '@tarojs/components';
import useFetchRequest from '../../../common/useFetchRequest';
import './detail.scss';

export default function SalesReportDetail(props) {
  const [report, setReport] = useState({});
  const FetchRequest = useFetchRequest();
  const { request, getNavigatorExtra } = FetchRequest
  const {splitImageFile,viewImageFiles } = FetchRequest

  useEffect(() => {
    const params = getNavigatorExtra(props.tid);
    request({
      url: 'saleReportItemQuery',
      data: { condition: params },
      callback: res => {
        const reportResult = { ...res.model };
        const {
          arriveTime,
          saleRecordTime,
          totalSaleQuantity,
          stallOrderQuantity,
        } = reportResult;

        const getTime = time => {
          time = time > 0 ? time : 0;
          var day = parseInt(time / 3600 / 24);
          var hour = parseInt((time / 3600) % 24);
          var minute = parseInt((time / 60) % 60);
          return `${day}天${hour}小时${minute}分`;
        };

        if (totalSaleQuantity >= stallOrderQuantity) {
          const time = (saleRecordTime - arriveTime) / 1000;
          reportResult.time = getTime(time);
        } else {
          const time = (new Date().getTime() - arriveTime) / 1000;
          reportResult.time = getTime(time);
        }

        res.success && setReport({ ...reportResult });
      },
    });
    // eslint-disable-next-line
  }, []);

  return (
    <View className='report-detail-page'>
      <View className='card-goods'>
        <Image
          className='goods-img img'
          src={splitImageFile({fileId:report.productCoverId,fileCode:report.productCoverCode})}
          onClick={viewImageFiles.bind(null,splitImageFile({fileId:report.productCoverId,fileCode:report.productCoverCode}))}
        />
        <View className='goods-info'>
          <View className='info'>货品名称: {report.productName}</View>
          <View className='info'>售卖市场: {report.marketName}</View>
          <View className='info'>订单编号: {report.stallOrderNo}</View>
          <View className='info'>销售时间: {report.time}</View>
        </View>
      </View>
      <View className='report-tables'>
        <View className='rows'>
          <View className='column'>
            <View>
              <Text className='ft-30'>{(report.stallOrderQuantity||0).toFixed(2)}</Text>
              <View className='label'>总件数</View>
            </View>
          </View>
          <View className='column'>
            <View>
              <Text className='ft-30'>{(report.stallOrderWeight||0).toFixed(2)}</Text>
              <View className='label'>总重量</View>
            </View>
          </View>
        </View>
        <View className='rows'>
          <View className='column'>
            <View>
              <Text className='ft-30'>{(report.totalSaleQuantity||0).toFixed(2)}</Text>
              <View className='label'>总销量</View>
            </View>
          </View>
          <View className='column'>
            <View>
              <Text className='ft-30'>{(report.totalSaleWeight||0).toFixed(2)}</Text>
              <View className='label'>总销售重量</View>
            </View>
          </View>
        </View>
        <View className='rows'>
          <View className='column'>
            <View>
              <Text className='ft-30'>{(report.daySaleQuantity||0).toFixed(2)}</Text>
              <View className='label'>今日销量</View>
            </View>
          </View>
          <View className='column'>
            <View>
              <Text className='ft-30'>{(report.daySaleWeight||0).toFixed(2)}</Text>
              <View className='label'>今日销售重量</View>
            </View>
          </View>
        </View>
        <View className='rows'>
          <View className='column'>
            <View>
              <Text className='ft-30'>{((report.stallOrderQuantity - report.totalSaleQuantity)||0).toFixed(2)}</Text>
              <View className='label'>剩余件数</View>
            </View>
          </View>
          <View className='column'>
            <View>
              <Text className='ft-30'>{((report.stallOrderWeight - report.totalSaleWeight)||0).toFixed(2)}</Text>
              <View className='label'>剩余重量</View>
            </View>
          </View>
        </View>

        <View className='rows'>
          <View className='column'>
            <View>
              <Text className='ft-30'>￥{(report.totalSaleAmount||0).toFixed(2)}</Text>
              <View className='label'>总销售金额</View>
            </View>
          </View>
          <View className='column'></View>
        </View>
      </View>

      <Navigator url={`/pages/gateManage/salesReport/sales?stallOrderId=${report.stallOrderId}`}>
        <View className='btn-extra'>
          <View className='btn'>查看详细销售明细</View>
        </View>
      </Navigator>
    </View>
  );
}
