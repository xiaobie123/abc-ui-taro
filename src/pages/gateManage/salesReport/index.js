import React, {  useEffect, useCallback } from 'react';
import { View, Text, Image, Block } from '@tarojs/components';
import Taro  from '@tarojs/taro';
import useFetchRequest from '../../../common/useFetchRequest';
import './index.scss';





export default function SalesReportList(props) {

  const FetchRequest = useFetchRequest();
  const { state, setAction, request, setUpdate } = FetchRequest;
  const { viewImageFiles, splitImageFile } = FetchRequest;
  const { reportList = [], reportExtra = {}  } = state;
  const { stallId } = props;
  // 获取列表数据
  const getReportItemList = useCallback(params => {
    request({
      url: 'saleReportItemList',
      data: params,
      callback: res => {
        res.success &&
          setAction({
            reportExtra: res.extra,
            reportList: res.models,
          });
        setUpdate();
      },
    });
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setAction({ types: 1 });
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (stallId) {
      const timestamp = new Date().getTime() - state.types * 24 * 3600 * 1000;
      getReportItemList({ condition: { stallId, startTime: timestamp } });
    }
    // eslint-disable-next-line
  }, [stallId, state.types]);

  // 切换
  const activeOnChange = useCallback(e => {
    const dataset = e.currentTarget.dataset;
    setUpdate(setAction({ types: dataset.index }));
    Taro.vibrateShort({})
     // eslint-disable-next-line
  }, []);

  return (
    <Block>
      {/**  列表展示  */}
      <View className='card-total'>
        <View className='time'>
          <Text className='ft-30'>交易数据</Text>
          <Text className='ft-26'> </Text>
        </View>
        <View className='info'>
          <View className='types'>
            <Text data-index={1} onClick={activeOnChange} className={(state.types==1&&'active')||'item'}>过去1天</Text>
            <Text data-index={7} onClick={activeOnChange} className={(state.types==7&&'active')||'item'}>过去7天</Text>
            <Text data-index={30} onClick={activeOnChange} className={(state.types==30&& 'active')||'item'}>过去30天</Text>
          </View>
          <View className='total'>
            <View className='total-item'>
              <Text className='ft-30'>￥{(reportExtra.totalSaleAmount || 0).toFixed(2)}</Text>
              <View className='label ft-26'>销售金额</View>
            </View>
            <View className='total-item'>
              <Text className='ft-30'>{(reportExtra.totalSaleQuantity || 0).toFixed(2)}</Text>
              <View className='label ft-26'>销售件数</View>
            </View>
          </View>
        </View>
      </View>
      {/** 列表展示 */}
      <View className='card-list'>
        <View className='card-title ft-30'>商品数据</View>
        {
          reportList && reportList.map((item,index)=>(
          <View key={index} onClick={()=>{
              Taro.navigateTo({url:'/pages/gateManage/salesReport/detail?stallOrderId='+item.stallOrderId})
            }}
          >
            <View className='card-goods'>
              <Image
                className='goods-img icon-img'
                src={splitImageFile({ fileId: item.productCoverId, fileCode: item.productCoverCode })}
                onClick={viewImageFiles.bind(null,splitImageFile({ fileId: item.productCoverId, fileCode: item.productCoverCode }))}
              />
              <View className='goods-info'>
                <View className='title ft-30'>{item.productName}</View>
                <View className='info'>{item.marketName} | {item.stallCode}</View>
                <View className='info'>订单编号: {item.stallOrderNo}</View>
              </View>
              <View className='total'>
                <View className='price ft-28'>￥{(item.totalSaleAmount||0).toFixed(2)}</View>
                <View className='quantity ft-26'>{(item.totalSaleQuantity||0).toFixed(2)} 件</View>
              </View>
            </View>
          </View>
        ))}
      </View>
    </Block>
  );
}
