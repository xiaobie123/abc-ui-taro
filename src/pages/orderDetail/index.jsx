import React, { useCallback, useEffect, useState } from 'react';
import { View } from '@tarojs/components';
import Taro, { usePullDownRefresh, useReachBottom, useRouter } from '@tarojs/taro';
// import { AtFloatLayout } from "taro-ui"
import PageContainer from '../../components/pageContainer';
import events from '../../utils/events';
import request from '../../common/request';
import VerticalSeparation from '../../components/VerticalSeparation';
// import PagefooterButton from '../../components/pagefooterButton';
// import Autograph from '../../components/canvos';
import StatusCard from './components/statusCard';
import SignForCard from './components/signForCard';
import DetailCom from './components/detailCom';
import './index.scss';

function Index() {
  const [item, setItem] = useState({});
  const [ stall, setStall] = useState({});
  const router = useRouter();
  const postData = useCallback(()=>{
    request({method : 'GET', tartget: 'orderStallOrderQuery', data: {condition: {id: router.params.id}} }).then((data)=>{
      if (data.success) {
        Taro.stopPullDownRefresh();
        setItem(data.model);
      }
    });
    request({method : 'GET', tartget: 'userStallUQuery', data: { condition: { id: router.params.stallId }} }).then((data)=>{
      if (data.success) {
        setStall(data.model);
      }
    });
  },[router.params.id, router.params.stallId]);

  // init
  useEffect(()=>{
    postData();
  },[postData]);

  // 新增编辑完毕后刷新首页列表
  useEffect(()=>{
    events.on('order-page-flush', function () {
      postData();
    })
  },[postData]);

  // 下拉刷新
  usePullDownRefresh(() => {
    postData();
  });

  useReachBottom(()=>{

  });

  const itemCardEdit = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/consignmentOrder/index?consignorOrderId=${item.consignorOrderId}&id=${item.id}&stallId=${item.stallId}&type=edit`,
    });
  },[item]);

  const itemCardCancel = useCallback(()=>{
    request({method : 'POST', tartget: 'orderStallOrderCancel', data: [{ id: item.id }]}).then((data)=>{
      if (data.success) {
        postData();// 更新当前模块
      }
    });
  },[item,postData]);


  const onRefuse = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/orderDetail/refuse?id=${item.id}&stallId=${item.stallId}&consignorOrderId=${item.consignorOrderId}`,
    });
  },[item]);

  const onSignFor = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/orderDetail/signFor?id=${item.id}&stallId=${item.stallId}&consignorOrderId=${item.consignorOrderId}`,
    });
    // Taro.navigateTo({
    //   url: `/pages/login/canvos`,
    // });
  },[item]);

  const onSeeSaleData = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/gateManage/salesData/history?stallOrderId=${item.id}`,
    });
  },[item]);

  const reportTodaySalesData = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/reporting/reportingEdit/index?stallOrderId=${item.id}`,
    });
  },[item]);
  // 此处状态复杂，用拼音标识名称，方便统计，
  const daichengdan = item.orderStatus === '10';// 待成单
  const daifahuo = item.orderStatus === '20';// 待发货
  const yifahuo = item.orderStatus === '30';// 待收货
  const yishouhuo = item.orderStatus === '41' || item.orderStatus === '42';// 已收货
  const isEdit = daichengdan || (daifahuo && item.consignorOrderTotalQuantity > item.consignorOrderSaleQuantity);
  return (
    <PageContainer noPadding>
      <VerticalSeparation />
      <StatusCard item={item} />
      {
        (yishouhuo || item.orderStatus === '50'|| item.orderStatus === '51') && (
          <>
            <VerticalSeparation />
            <SignForCard item={item} />
          </>
        )
      }
      <DetailCom item={item} stall={stall} />
      <VerticalSeparation height={68} />
      {
        !(yifahuo || daifahuo) && (
          <View className='footerButtonWap'>
            <View className='orderDetail-button'>
              {
                isEdit &&
                (
                  <View className='button-default' onClick={itemCardEdit}>
                    修改订单
                  </View>
                )
              }
              {
                isEdit &&
                (
                  <View
                    className='button-default'
                    onClick={()=>{
                      Taro.showModal({
                        title: '确认取消',
                        content: '确定要取消吗?',
                        success: (res) => {
                          if (res.confirm) {
                            itemCardCancel();
                          }
                        },
                      });
                    }}
                  >
                    取消订单
                  </View>
                )
              }
              {
                item.orderStatus === '40' &&
                (
                  <>
                    <View className='button-primary' onClick={onSignFor}>确认验收</View>
                    <View className='button-primary' onClick={onRefuse}>货品拒收</View>
                  </>
                )
              }
              {
                yishouhuo &&
                (
                  <View className='button-primary' onClick={onSeeSaleData}>
                    查看销售数据
                  </View>
                )
              }
              {
                (yishouhuo && !(item.isSaleOut === 1) ) &&
                (<View className='button-primary' onClick={reportTodaySalesData}>
                  上报今日销售数据
                </View>)
              }
            </View>
          </View>
        )
      }
    </PageContainer>
  )
}

export default Index;
