import React, { useCallback, useEffect, useRef, useState } from 'react';
import { View } from '@tarojs/components';
import Taro, { usePullDownRefresh, useReachBottom, useRouter } from '@tarojs/taro';
import PageContainer from '../../components/pageContainer';
import StatusCard from './components/statusCard';
import events from '../../utils/events';
import './index.scss';
import request from '../../common/request';
import VerticalSeparation from '../../components/VerticalSeparation';
import FileUpload from '../../components/fileUpload';
import DetailCom from './components/detailCom';
import PagefooterButton from '../../components/pagefooterButton';
import Autograph from '../../components/autograph';

function Index() {
  const [item, setItem] = useState({});
  const [ stall, setStall] = useState({});
  const [consignorOrderProduct, setConsignorOrderProduct] = useState();
  const refFile = useRef(null);
  const autographRef = useRef(null);
  const router = useRouter();
  const postData = useCallback(()=>{
    request({method : 'GET', tartget: 'orderStallOrderQuery', data: {condition: {id: router.params.id}} }).then((data)=>{
      if (data.success) {
        setItem(data.model);
      }
    });
    request({method : 'GET', tartget: 'userStallUQuery', data: { condition: { id: router.params.stallId }} }).then((data)=>{
      if (data.success) {
        setStall(data.model);
      }
    });
    request({method : 'GET', tartget: 'consignorOrderProductQuery', data: {condition: {id: router.params.consignorOrderId}} }).then((data)=>{
      if (data.success) {
        setConsignorOrderProduct(data.model);
      }
    });
  },[router.params.consignorOrderId, router.params.id, router.params.stallId]);

  const onSubmit = useCallback(()=>{
    if (refFile.current.getFileList().length === 0) {
      Taro.showToast({
        title: '请上传所签收货品的照片',
        icon: 'none',
      });
      return;
    }
    if (!autographRef.current.getFile().fileId) {
      Taro.showToast({
        title: '请签名',
        icon: 'none',
      });
      return;
    }
    const parms = {
      // stallOrderSpecList: item.stallOrderSpecList,
      // stallId: stall.id,
      stallOrderId: item.id,
      // consignorOrderId: consignorOrderProduct.id,
      // productId:consignorOrderProduct.productId,
        //
      stallOrderReceivingSignatureList:[autographRef.current.getFile()],
      stallOrderReceivingFileList: refFile.current.getFileList(),
    };
    request({method : 'POST', tartget: 'orderStallOrderSign', data:[parms]}).then((data)=>{
      if (data.success) {
        events.emit('order-page-flush',{});
        Taro.navigateBack();
      }
    });
  },[item]);

  // init
  useEffect(()=>{
    postData();
  },[postData]);

  // 新增编辑完毕后刷新首页列表
  useEffect(()=>{
    events.on('order-page-flush', function () {
      postData();
    })
  },[postData]);

  // 下拉刷新
  usePullDownRefresh(() => {
    postData();
  });

  useReachBottom(()=>{

  });

  return (
    <PageContainer noPadding>
      <VerticalSeparation />
      <StatusCard item={item} />
      <DetailCom item={item} stall={stall} />
      <FileUpload require label='请上传所签收货品的照片' actionRef={refFile} />
      <Autograph actionRef={autographRef} />
      <View style={{ height: '100px' }} />
      <PagefooterButton
        oneButton={{
          text:'确认已收货',
          onClick:onSubmit,
        }}
      />
    </PageContainer>
  )
}

export default Index;
