import React, { useCallback } from 'react';
import { Image, View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import localPng from '../../../assets/local.png';
import myicon3Png from '../../../assets/myicon3.png';
import CardContainer from '../../../components/cardContainer'
import './index.scss';
import { getImagePath } from '../../../utils';
import VerticalSeparation from '../../../components/VerticalSeparation';


function Index(props) {
  const { item={}, stall={} } = props;
  const logisticsDeliveryList = item.logisticsDeliveryList || [];
// 此处状态复杂，用拼音标识名称，方便统计，
  const yifahuo = item.orderStatus === '30';// 待收货
  return (
    <>
      {
        yifahuo && (
          <>
            <CardContainer>
              {
                logisticsDeliveryList.map((i)=>{
                  if (i.deliveryType === 0) {
                    return (
                      <View className='top'>
                        <Image
                          className='left-img myicon3Png'
                          src={myicon3Png}
                        />
                        <View className='middle'>
                          <View>
                            <Text className='middle-text1'>{i.driverName}</Text>
                            <Text className='middle-text2'>{i.driverPhone}</Text>
                          </View>
                          <View>{i.vehicleInformation} | {i.plateNumber}</View>
                        </View>
                      </View>
                    );
                  } else {
                    return (
                      <View className='top'>
                        <View className='middle'>
                          <View>
                            <Text className='middle-text1'>{i.logisticsCompany}</Text>
                          </View>
                          <View>{i.logisticsOrderNo}</View>
                        </View>
                        <Image
                          className='middle-img'
                          onClick={()=>{
                            Taro.previewImage({
                              current: getImagePath(i.logisticsDeliveryFileList[0]?.fileId,i.logisticsDeliveryFileList[0]?.fileCode),
                              urls: [getImagePath(i.logisticsDeliveryFileList[0]?.fileId,i.logisticsDeliveryFileList[0]?.fileCode)],
                            });
                          }}
                          src={
                            getImagePath(i.logisticsDeliveryFileList[0]?.fileId,i.logisticsDeliveryFileList[0]?.fileCode)
                          }
                        />
                      </View>
                    );
                  }
                })
              }
              <VerticalSeparation height={22} />
            </CardContainer>
            <VerticalSeparation height={22} />
          </>
        )
      }
      <CardContainer>
        <View className='top'>
          <Image
            className='left-img'
            src={localPng}
          />
          <View className='middle'>
            <View>
              <Text className='middle-text1'>{stall.receiverName}</Text>
              <Text className='middle-text2'>{stall.receiverPhoneNumber}</Text>
            </View>
            <View>{stall.marketName} | {stall.stallCode}</View>
          </View>
        </View>
        <VerticalSeparation height={22} />
      </CardContainer>
    </>
  );
}

export default React.memo(Index);
