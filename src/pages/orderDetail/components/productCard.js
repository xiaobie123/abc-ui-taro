import React, { useEffect, useState } from 'react';
import { Image, View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './index.scss';
import { getImagePath, formatTimeDate } from '../../../utils';
import defaultHeaderPng from '../../../assets/default_header.png';


function Index(props) {
  const [ all, setAll] = useState(0);
  const { item={} } = props;
  useEffect(()=>{
    let allTem = 0;
    item.stallOrderSpecList&& item.stallOrderSpecList.map((i)=>{
      allTem += parseFloat(i.reservedQuantity);
    });
    setAll(allTem);
  },[item.stallOrderSpecList]);
  const userFilePath = item.consignorUserAvatar || defaultHeaderPng;
  return (
    <View className='itemCard-order'>
      <View className='itemCard-title'>
        <View className='itemCard-title-left'>
          <Image
            onClick={()=>{
              Taro.previewImage({
                current: userFilePath,
                urls: [userFilePath],
              });
            }}
            className='img'
            src={userFilePath}
          />
          <Text className='text1'>{item.consignorSubjectName}</Text>
        </View>
        <View className='itemCard-title-status'>
        </View>
      </View>
      <View className='itemCard-message'>
        {
          item.productFileId && (
            <Image
              className='itemCard-message-left'
              onClick={()=>{
                Taro.previewImage({
                  current: getImagePath(item.productFileId,item.productFileCode),
                  urls: [getImagePath(item.productFileId,item.productFileCode)],
                });
              }}
              src={getImagePath(item.productFileId,item.productFileCode)}
            />
          )
        }
        <View className='itemCard-message-right'>
          <View className='text1'>{item.productName}</View>
          <View className='text2'>{`${item?.orderProduct?.originRegion} ${item?.orderProduct?.originCity} ${item?.orderProduct?.originArea}`}</View>
          <View className='text2'>预计{formatTimeDate(item.expectArriveTime)}到货</View>
        </View>
      </View>
      <View className='itemCard-guiGe-row'>
        <View className='itemCard-row-left'>
          <Text className='text1'>
            预约量
          </Text>
        </View>
        <View className='itemCard-row-right'>
          共计{all}件
        </View>
      </View>
      {
        item.stallOrderSpecList &&
        item.stallOrderSpecList.map((i)=>{
          return (
            <View className='itemCard-guiGe-row'>
              <View className='itemCard-row-left'>
                <Text className='text1'>
                  {i.productSpecName}
                </Text>
                {/* <Text className='text2'>

                </Text> */}
              </View>
              <View className='itemCard-row-right'>
                x{i.reservedQuantity}
              </View>
            </View>
          );
        })
      }
      {/* <View className='itemCard-money'>
        <Text className='text1'>
          订单金额：
        </Text>
        <Text className='text1'>
          ¥600255.00
        </Text>
      </View> */}
      {/* <View className='itemCard-result-status'>
        <Image
          className='img'
          src={shuiguoPng}
        />
        <Text className='text'>
          售卖已完成
        </Text>
      </View> */}
      <View className='itemCard-fenGe'  />
    </View>
  );
}

export default React.memo(Index);
