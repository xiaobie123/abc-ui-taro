import React, { useCallback } from 'react';
import { View } from '@tarojs/components';
import Taro from '@tarojs/taro';
import FlowingWaterCard from './flowingWaterCard';
import StallCard from './stallCard';
import ProductCard from './productCard';
import VerticalSeparation from '../../../components/VerticalSeparation';
import './index.scss';


function Index(props) {
  const { item={}, stall={} } = props;

  return (
    <View>
      <VerticalSeparation />
      <StallCard item={item} stall={stall} />
      <VerticalSeparation />
      <ProductCard item={item} />
      <FlowingWaterCard item={item} />
    </View>
  );
}

export default React.memo(Index);
