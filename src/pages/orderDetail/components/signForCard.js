import React from 'react';
import { Image, View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import { getImagePath, formatTimeDate, formatNumber } from '../../../utils';

import CardContainer from '../../../components/cardContainer'
import './index.scss';
import VerticalSeparation from '../../../components/VerticalSeparation';

function formatTimeMinute(date) {
  if (!date || date < 0) {
    return;
  }
  date = new Date(date);
  var hour = date.getHours();
  var minute = date.getMinutes();
  // var seconds = date.getSeconds();

  return [hour, minute].map(formatNumber).join(':');
}

function Index(props) {
  const { item={} } = props;

  const stallOrderReceiving = item.stallOrderReceiving || {};
  // todo  签收人要判断
  return (
    <CardContainer>
      <VerticalSeparation />
      <View className='signForCard'>
        <View className='signForCard-top'>
          {
            (item.orderStatus === '50' || item.orderStatus === '51') ? (
              <View className='left-text'>
                <View className='text'>拒收人：{item.orderStatus === '50' ? item?.consignorOrderReceiving?.createName : item?.stallOrderReceiving?.createName}</View>
                <View className='text'>拒收理由：{item.orderStatus === '50' ? item?.consignorOrderReceiving?.rejectionReason: stallOrderReceiving.rejectionReason}</View>
                <View className='text'>拒收时间：{
                  item.orderStatus === '50' ?
                    `${formatTimeDate(item?.consignorOrderReceiving?.receivingTime)} ${formatTimeMinute(item?.consignorOrderReceiving?.receivingTime)}`
                    :
                  `${formatTimeDate(stallOrderReceiving.receivingTime)} ${formatTimeMinute(stallOrderReceiving.receivingTime)}`
                }
                </View>
                <View className='text'></View>
              </View>
            ) : (
              <View className='left-text'>
                <View className='text'>签收人：{
                  item?.stallOrderReceiving?.createName
                }</View>
                <View className='text'>
                  签收时间：
                  <Text className='text'>{formatTimeDate(item?.stallOrderReceiving.receivingTime)} {formatTimeMinute(item?.stallOrderReceiving.receivingTime)}</Text>
                </View>
              </View>
            )
          }
          {
            item.orderStatus === '50' ? (
              <Image className='left-img'
                     onClick={()=>{
                       Taro.previewImage({
                         current: getImagePath(item?.consignorOrderReceiving?.consignorOrderReceivingSignatureList[0].fileId,item?.consignorOrderReceiving?.consignorOrderReceivingSignatureList[0].fileCode),
                         urls: [getImagePath(item?.consignorOrderReceiving?.consignorOrderReceivingSignatureList[0].fileId,item?.consignorOrderReceiving?.consignorOrderReceivingSignatureList[0].fileCode)],
                       });
                     }}
                     src={getImagePath(item?.consignorOrderReceiving?.consignorOrderReceivingSignatureList[0].fileId,item?.consignorOrderReceiving?.consignorOrderReceivingSignatureList[0].fileCode)}
              />
            ): (
              <Image className='left-img'
                     onClick={()=>{
                       Taro.previewImage({
                         current: getImagePath(stallOrderReceiving?.stallOrderReceivingSignatureList[0].fileId,stallOrderReceiving?.stallOrderReceivingSignatureList[0].fileCode),
                         urls: [getImagePath(stallOrderReceiving?.stallOrderReceivingSignatureList[0].fileId,stallOrderReceiving?.stallOrderReceivingSignatureList[0].fileCode)],
                       });
                     }}
                     src={getImagePath(stallOrderReceiving?.stallOrderReceivingSignatureList[0].fileId,stallOrderReceiving?.stallOrderReceivingSignatureList[0].fileCode)}
              />
            )
          }
        </View>
        <View className='img'>
          {
            item.orderStatus === '50' ? (
              item?.consignorOrderReceiving?.consignorOrderReceivingFileList?.map((i)=>{
                return (
                  <Image
                    onClick={()=>{
                      Taro.previewImage({
                        current: getImagePath(i.fileId,i.fileCode),
                        urls: [getImagePath(i.fileId,i.fileCode)],
                      });
                    }}
                    className='img-item' src={getImagePath(i.fileId,i.fileCode)}
                  />
                )
              })
            ):(
              stallOrderReceiving?.stallOrderReceivingFileList?.map((i)=>{
                return (
                  <Image
                    onClick={()=>{
                      Taro.previewImage({
                        current: getImagePath(i.fileId,i.fileCode),
                        urls: [getImagePath(i.fileId,i.fileCode)],
                      });
                    }}
                    className='img-item' src={getImagePath(i.fileId,i.fileCode)}
                  />
                )
              })
            )
          }
        </View>
      </View>
      <VerticalSeparation />
    </CardContainer>
  );
}

export default React.memo(Index);
