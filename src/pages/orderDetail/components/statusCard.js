import React from 'react';
import { View, Text } from '@tarojs/components';
// import Taro from '@tarojs/taro';
import { deadlineTime, formatNumber, pastTime, diffTime } from '../../../utils';

import './index.scss';
// const statusMap = {
//   '0': '待成单',
//   '1': '待发货',
//   '2': '待收货',
//   '3': '已收货',
//   '4': '待发布',
//   '5': '已取消',
//   '6': '市场已拒收',
//   '7': '档主已拒收',
// };
/*
* DRAFT("00", "待发布"),
    COLLAGING("10", "待成单"),
    NO_DELIVER("20", "待发货"),
    DELIVERING("30", "待收货"),
    RECEIVED_MARKET("40", "待收货，市场签收"),
    RECEIVED_STALL("41", "已收货，档主主动签收"),
    RECEIVED_SYSTEM("42", "已收货，到期系统自动签收"),
    REJECTED_MARKET("50", "已拒收，市场拒收"),
    REJECTED_STALL("51", "已拒收，档主拒收"),
    CANCELED_CONSIGNOR_COLLAGE("60", "已取消，待成单货主用户主动取消"),
    CANCELED_CONSIGNOR_DELIVER("61", "已取消，待发货货主用户主动取消"),
    CANCELED_STALL_COLLAGE("62", "已取消，待成单档主用户主动取消"),
    CANCELED_STALL_DELIVER("63", "已取消，待发货档用户主动取消"),
    CANCELED_SYSTEM_COLLAGE("64", "已取消，无成单到期系统自动取消"),
    CANCELED_SYSTEM_DELIVER("65", "已取消，未发货到期系统自动取消");
* */

const statusMap = {
  '60': {
    status:'已取消',
    content: '很抱歉，订单已被货主主动取消',// 已取消，待成单货主用户主动取消
  },
  '61': {
    status:'已取消',
    content: '很抱歉，订单已被货主主动取消',// 已取消，待发货货主用户主动取消
  },
  '62': {
    status:'已取消',
    content: '很抱歉，订单已被档主主动取消',// 已取消，待成单档主用户主动取消
  },
  '63': {
    status:'已取消',
    content: '很抱歉，订单已被档主主动取消',// 已取消，待发货档用户主动取消
  },
  '64': {
    status:'已取消',
    content: '很抱歉，订单已被系统自动取消',// 已取消，无成单到期系统自动取消
  },
  '65': {
    status:'已取消',
    content: '很抱歉，订单已被系统自动取消',// 已取消，未发货到期系统自动取消
  },
};
// 发货截止时间   成单时间
const ycfh = (deliveryDeadline,achievingTime) =>{
  if (!deliveryDeadline || !achievingTime) {
    return ''
  }
  // let diff = new Date().getTime() -(deliveryDeadline - (achievingTime + 48 * 60 * 60 * 1000));
  let diff = deliveryDeadline - (achievingTime + 48 * 60 * 60 * 1000);

  let baseTime = 24 * 60 * 60 * 1000; // 天基数
  const day = Math.floor(diff / baseTime);
  diff = diff % baseTime;
  baseTime = baseTime / 24;
  let hour = Math.floor(diff / baseTime);
  diff = diff % baseTime;
  baseTime = baseTime / 60;
  const minute = Math.floor(diff / baseTime);
  diff = diff % baseTime;
  baseTime = baseTime / 60;
  const seconds = Math.floor(diff / baseTime);

  hour += day * 24;
  return [hour, minute, seconds]
    .map(formatNumber)
    .join(':')
    .split('');
}

function Index(props) {
  const { item={} } = props;

  if (item.orderStatus === '10') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          待成单
        </View>
        {
          item.isDelayed ? (
            <View className='text2'>
              还剩 {deadlineTime(item.deadline)} 订单生效 (已延长)
            </View>
          ):(
            <View className='text2'>
              还剩 {deadlineTime(item.deadline)} 订单生效
            </View>
          )
        }
      </View>
    );
  }

  if (item.orderStatus === '20') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          待发货
        </View>
        <View className='text2'>
          该订单还剩 {deadlineTime(item.deliveryDeadline)} 失效
          {
            !!item.isDeliveryDelayed && <Text>(已延迟发货{diffTime(item.deliveryDeadline,item.achievingTime+ 48 * 60 * 60 * 1000)})</Text>
          }
        </View>
      </View>
    );
  }
  if (item.orderStatus === '30') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          待收货
        </View>
        <View className='text2'>
          {/* todo */}
          还剩 {deadlineTime(item.receiveDeadline)} 自动确认收货
        </View>
      </View>
    );
  }
  if (item.orderStatus === '40') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          待收货
        </View>
        <View className='text2'>
          市场签收
        </View>
      </View>
    );
  }
  if (item.orderStatus === '41') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          已收货
        </View>
        <View className='text2'>
          已收货，档主主动签收
        </View>
      </View>
    );
  }
  if (item.orderStatus === '42') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          已收货
        </View>
        <View className='text2'>
          已收货，到期系统自动签收
        </View>
      </View>
    );
  }
  if (item.orderStatus === '50') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          已拒收
        </View>
        <View className='text2'>
          已被市场拒收
        </View>
      </View>
    );
  }
  if (item.orderStatus === '51') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          已拒收
        </View>
        <View className='text2'>
          已被档主拒收
        </View>
      </View>
    );
  }
  if (item.orderStatus === '52') {
    return (
      <View className='statusCard'>
        <View className='text1'>
          已拒收
        </View>
        <View className='text2'>
          未铺货到期系统自动拒收
        </View>
      </View>
    );
  }
  return (
    <View className='statusCard'>
      <View className='text1'>
        {statusMap[item.orderStatus]?.status}
      </View>
      <View className='text2'>
        {statusMap[item.orderStatus]?.content}
      </View>
    </View>
  );
}

export default React.memo(Index);
