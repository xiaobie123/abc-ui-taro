import React from 'react';
import { View } from '@tarojs/components';
import { formatTimeDate, formatTimeTime  } from '../../../utils';
import './index.scss';

// const statusMap = {   原状态
//   '0': '待成单',
//   '1': '待发货',
//   '2': '待收货',
//   '3': '已收货',
//   '4': '待发布',
//   '5': '已取消',
//   '6': '市场已拒收',
//   '7': '档主已拒收',
// };
/*
* DRAFT("00", "待发布"),
    COLLAGING("10", "待成单"),
    NO_DELIVER("20", "待发货"),
    DELIVERING("30", "待收货"),
    RECEIVED_MARKET("40", "待收货，市场签收"),
    RECEIVED_STALL("41", "已收货，档主主动签收"),1
    RECEIVED_SYSTEM("42", "已收货，到期系统自动签收"),1
    REJECTED_MARKET("50", "已拒收，市场拒收"),1
    REJECTED_STALL("51", "已拒收，档主拒收"),1
    CANCELED_CONSIGNOR_COLLAGE("60", "已取消，待成单货主用户主动取消"),
    CANCELED_CONSIGNOR_DELIVER("61", "已取消，待发货货主用户主动取消"),
    CANCELED_STALL_COLLAGE("62", "已取消，待成单档主用户主动取消"),
    CANCELED_STALL_DELIVER("63", "已取消，待发货档用户主动取消"),
    CANCELED_SYSTEM_COLLAGE("64", "已取消，无成单到期系统自动取消"),
    CANCELED_SYSTEM_DELIVER("65", "已取消，未发货到期系统自动取消");
* */
function Index(props) {
  const { item={} } = props;
  // 此处状态复杂，用拼音标识名称，方便统计，
  const daichengdan = item.orderStatus === '10';// 待成单
  const daifahuo = item.orderStatus === '20';// 待发货
  const yifahuo = item.orderStatus === '30';// 待收货
  const yishouhuo = item.orderStatus === '41' || item.orderStatus === '42';// 已收货
  // const yijushou = item.orderStatus === '50' || item.orderStatus === '51';// 已拒收
  const shichangyijushou = item.orderStatus === '50' || item.orderStatus === '52';// 已拒收 市场拒收
  const dangzhuyijushou = item.orderStatus === '51';// 已拒收 档主拒收
  const huozhuquxiao = item.orderStatus === '60' || item.orderStatus === '61'|| item.orderStatus === '64';// 货主取消
  const dangzhuquxiao = item.orderStatus === '62' || item.orderStatus === '63'|| item.orderStatus === '65';// 档主取消
  const yiquxiao = huozhuquxiao || dangzhuquxiao;// 已取消
  return (
    <View className='flowingWaterCard'>
      <View className='text'>代卖编号: {item.consignorOrderNo}</View>
      <View className='text'>订单编号: {item.stallOrderNo}</View>
      <View className='text'>预订时间: {`${formatTimeDate(item.createTime)} ${formatTimeTime(item.createTime)}`}</View>
      {
        !daichengdan && <View className='text'>成单时间: {`${formatTimeDate(item.achievingTime) || ''} ${formatTimeTime(item.achievingTime) || ''}`}</View>
      }
      {
        (daichengdan || daifahuo) && (
          <>
            <View className='text'>预计发货时间: {formatTimeDate(item.expectDeliveryTime)}</View>
          </>
        )
      }
      {
        (!daichengdan && !daifahuo && !yiquxiao) && (
          <>
            <View className='text'>发货时间: {formatTimeDate(item.deliveryTime)} {formatTimeTime(item.deliveryTime) || ''}</View>
          </>
        )
      }
      {
        (daichengdan || daifahuo || yifahuo) && (
          <>
            <View className='text'>预计到货时间: {formatTimeDate(item.expectArriveTime)}</View>
          </>
        )
      }
      {
        (dangzhuyijushou) && (
          <View className='text'>档口拒收时间: {formatTimeDate(item?.stallOrderReceiving?.receivingTime)} {formatTimeTime(item?.stallOrderReceiving?.receivingTime) || ''}</View>
        )
      }
      {
        (shichangyijushou) && (
          <View className='text'>市场拒收时间: {formatTimeDate(item?.consignorOrderReceiving?.receivingTime)} {formatTimeTime(item?.consignorOrderReceiving?.receivingTime) || ''}</View>
        )
      }
      {
        (yishouhuo) && (
          <>
            <View className='text'>到货时间: {formatTimeDate(item.receiveTime)} {formatTimeTime(item.receiveTime) || ''}</View>
          </>
        )
      }
      {
        (yiquxiao) && (
          <View className='text'>取消时间: {formatTimeDate(item.updateTime)} {formatTimeTime(item.updateTime) || ''}</View>
        )
      }
    </View>
  );
}

export default React.memo(Index);
