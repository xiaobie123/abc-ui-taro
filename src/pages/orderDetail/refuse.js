import React, { useCallback, useEffect, useRef, useState } from 'react';
import { View, Text, Textarea } from '@tarojs/components';
import Taro, { usePullDownRefresh, useReachBottom, useRouter } from '@tarojs/taro';
import PageContainer from '../../components/pageContainer';
import StatusCard from './components/statusCard';
// import StallCard from './components/stallCard';
// import ProductCard from './components/productCard';
// import FlowingWaterCard from './components/flowingWaterCard';
import events from '../../utils/events';
import './index.scss';
import request from '../../common/request';
import VerticalSeparation from '../../components/VerticalSeparation';
import FileUpload from '../../components/fileUpload';
import DetailCom from './components/detailCom';
import PagefooterButton from '../../components/pagefooterButton';
import Autograph from '../../components/autograph';

function Index() {
  const [item, setItem] = useState({});
  const [ stall, setStall] = useState({});
  const [consignorOrderProduct, setConsignorOrderProduct] = useState();
  const [ refuseValue, setRefuseValue] = useState(null);
  const refFile = useRef(null);
  const autographRef = useRef(null);
  const router = useRouter();
  const postData = useCallback(()=>{
    request({method : 'GET', tartget: 'orderStallOrderQuery', data: {condition: {id: router.params.id}} }).then((data)=>{
      if (data.success) {
        setItem(data.model);
      }
    });
    request({method : 'GET', tartget: 'userStallUQuery', data: { condition: { id: router.params.stallId }} }).then((data)=>{
      if (data.success) {
        setStall(data.model);
      }
    });
    request({method : 'GET', tartget: 'consignorOrderProductQuery', data: {condition: {id: router.params.consignorOrderId}} }).then((data)=>{
      if (data.success) {
        setConsignorOrderProduct(data.model);
      }
    });
  },[]);

  const onSubmit = useCallback(()=>{
    if (!refuseValue) {
      Taro.showToast({
        title: '请填写拒收理由',
        icon: 'none',
      });
      return;
    }
    if (refFile.current.getFileList().length === 0) {
      Taro.showToast({
        title: '请上传要拒收货品的照片',
        icon: 'none',
      });
      return;
    }
    if (!autographRef.current.getFile().fileId) {
      Taro.showToast({
        title: '请签名',
        icon: 'none',
      });
      return;
    }
    const parms = {
      stallOrderId: item.id,
      rejectionReason: refuseValue,
      stallOrderReceivingSignatureList:[autographRef.current.getFile()],
      stallOrderReceivingFileList: refFile.current.getFileList(),
    };
    request({method : 'POST', tartget: 'orderStallOrderReject', data:[parms]}).then((data)=>{
      if (data.success) {
        events.emit('order-page-flush',{});
        Taro.navigateBack();
      }
    });
  },[item,consignorOrderProduct, refuseValue]);

  // init
  useEffect(()=>{
    postData();
  },[]);

  // 新增编辑完毕后刷新首页列表
  useEffect(()=>{
    events.on('order-page-flush', function () {
      postData();
    })
  },[]);

  // 下拉刷新
  usePullDownRefresh(() => {
    postData();
  });

  useReachBottom(()=>{

  });

  const inputOnChange = (e)=>{
    const inputValue = e.target.value;
    setRefuseValue(inputValue);
  }

  return (
    <PageContainer noPadding>
      <VerticalSeparation />
      <StatusCard item={item} />
      <DetailCom item={item} stall={stall} />
      <VerticalSeparation />
      <View className='refuse-input'>
        <View className='title'>
          <Text className='itemRequire'>*</Text>
          请填写拒收理由
        </View>
        <Textarea
          placeholderStyle='color:#CCCCCC'
          placeholder='请输入'
          onInput={inputOnChange}
          value={refuseValue}
          className='input'
          autoFocus
        />
      </View>
      <FileUpload require label='请上传要拒收货品的照片' actionRef={refFile} />
      <Autograph actionRef={autographRef} />
      <VerticalSeparation height={58} />
      <PagefooterButton
        oneButton={{
          text:'确认档主拒收',
          onClick:onSubmit,
        }}
      />
    </PageContainer>
  )
}

export default Index;
