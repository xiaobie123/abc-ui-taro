import React, { useCallback, useEffect, useState } from 'react';
import { Image, View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import request from '../../../common/request';
import defaultHeaderPng from '../../../assets/default_header.png';
import { getImagePath } from '../../../utils/index';
import './itemCard.scss';

// const statusMap = {
//   '0': '待成单',
//   '1': '待发货',
//   '2': '待收货',
//   '3': '已收货',
//   '4': '待发布',
//   '5': '已取消',
//   '6': '市场已拒收',
//   '7': '档主已拒收',
// };
/*
* DRAFT("00", "待发布"),
    COLLAGING("10", "待成单"),
    NO_DELIVER("20", "待发货"),
    DELIVERING("30", "待收货"),
    RECEIVED_MARKET("40", "待收货，市场签收"),
    RECEIVED_STALL("41", "已收货，档主主动签收"),
    RECEIVED_SYSTEM("42", "已收货，到期系统自动签收"),
    REJECTED_MARKET("50", "已拒收，市场拒收"),
    REJECTED_STALL("51", "已拒收，档主拒收"),
    CANCELED_CONSIGNOR_COLLAGE("60", "已取消，待成单货主用户主动取消"),
    CANCELED_CONSIGNOR_DELIVER("61", "已取消，待发货货主用户主动取消"),
    CANCELED_STALL_COLLAGE("62", "已取消，待成单档主用户主动取消"),
    CANCELED_STALL_DELIVER("63", "已取消，待发货档用户主动取消"),
    CANCELED_SYSTEM_COLLAGE("64", "已取消，无成单到期系统自动取消"),
    CANCELED_SYSTEM_DELIVER("65", "已取消，未发货到期系统自动取消");
* */
const statusMap = {
  // '00': '待发布',
  '10': '待成单',
  '20': '待发货',
  '30': '待收货',
  '40': '待收货',
  '41': '已收货',// ，档主主动签收
  '42': '已收货',// ，到期系统自动签收
  '50': '已拒收',// ，市场拒收
  '51': '已拒收',// ，市场拒收
  '60': '已取消',
  '61': '已取消',
  '62': '已取消',
  '63': '已取消',
  '64': '已取消',
  '65': '已取消',
};
function Index(props) {
  const { parms={} } = props;
  const [ item, setItem] = useState({});

  useEffect(()=>{
    setItem(parms);
  },[parms]);

  const itemCardDetail = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/orderDetail/index?id=${item.id}&stallId=${item.stallId}&consignorOrderId=${item.consignorOrderId}`,
    });
  },[item]);

  const itemCardEdit = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/consignmentOrder/index?consignorOrderId=${item.consignorOrderId}&id=${item.id}&stallId=${item.stallId}&type=edit`,
    });
  },[item]);

  const itemCardCancel = useCallback(()=>{
    request({method : 'POST', tartget: 'orderStallOrderCancel', data: [{ id: item.id }]}).then((data)=>{
      if (data.success) {
        setItem({...item, orderStatus: '60'});// 更新当前模块
      }
    });
  },[item]);

  const onRefuse = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/orderDetail/refuse?id=${item.id}&stallId=${item.stallId}&consignorOrderId=${item.consignorOrderId}`,
    });
  },[item]);

  const onSignFor = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/orderDetail/signFor?id=${item.id}&stallId=${item.stallId}&consignorOrderId=${item.consignorOrderId}`,
    });
  },[item]);

  const onSeeSaleData = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/gateManage/salesData/history?stallOrderId=${item.id}`,
    });
  },[item]);

  const reportTodaySalesData = useCallback(()=>{
    Taro.navigateTo({
      url: `/pages/reporting/reportingEdit/index?stallOrderId=${item.id}`,
    });
  },[item]);

  // 此处状态复杂，用拼音标识名称，方便统计，
  const daichengdan = item.orderStatus === '10';// 待成单
  const daifahuo = item.orderStatus === '20';// 待发货
  // const yifahuo = item.orderStatus === '30';// 待收货
  const yishouhuo = item.orderStatus === '41' || item.orderStatus === '42';// 已收货
  // const yijushou = item.orderStatus === 50 || item.orderStatus === 51;// 已拒收

  const isEdit = daichengdan || (daifahuo && item.consignorOrderTotalQuantity > item.consignorOrderSaleQuantity);
  const productFilePath = getImagePath(item.productFileId,item.productFileCode);
  const userFilePath = item.consignorUserAvatar || defaultHeaderPng;
  console.log(userFilePath);
  return (
    <View className='itemCard-order'>
      <View className='itemCard-title'>
        <View className='itemCard-title-left'>
          <Image
            onClick={()=>{
              Taro.previewImage({
                current: userFilePath,
                urls: [userFilePath],
              });
            }}
            className='img'
            src={userFilePath}
          />
          <Text className='text1'>{item.consignorSubjectName}</Text>
        </View>
        <View className='itemCard-title-status'>
          {statusMap[item.orderStatus]}
        </View>
      </View>
      <View className='itemCard-message'>
        <Image
          onClick={()=>{
            Taro.previewImage({
              current: productFilePath,
              urls: [productFilePath],
            });
          }}
          className='itemCard-message-left'
          src={productFilePath}
        />
        <View className='itemCard-message-right' onClick={itemCardDetail}>
          <View className='text1'>{item.productName}</View>
          {/* <View className='text2'>预计{formatTimeDate(item.expectArriveTime)}到货</View> */}
        </View>
      </View>
      {
        item.stallOrderSpecList &&
        item.stallOrderSpecList.map((i)=>{
          return (
            <View className='itemCard-guiGe-row' onClick={itemCardDetail}>
              <View className='itemCard-row-left'>
                <Text className='text1'>
                  {i.productSpecName}
                </Text>
              </View>
              <View className='itemCard-row-right'>
                x{i.reservedQuantity}
              </View>
            </View>
          );
        })
      }
      <View className='itemCard-fenGe'  />
      <View className='itemCard-button'>
        <View className='button-default' onClick={itemCardDetail}>
          预订详情
        </View>
        {
          isEdit &&
          (
            <View className='button-default' onClick={itemCardEdit}>
              修改订单
            </View>
          )
        }
        {
          isEdit &&
          (
            <View
              className='button-default'
              onClick={()=>{
                Taro.showModal({
                  title: '确认取消',
                  content: '确定要取消吗?',
                  success: (res) => {
                    if (res.confirm) {
                      itemCardCancel();
                    }
                  },
                });
              }}
            >
              取消订单
            </View>
          )
        }
        {
          item.orderStatus === '40' && (
            <>
            <View className='button-default' onClick={onSignFor}>
               确认验收
            </View>
            <View className='button-default' onClick={onRefuse}>
              货品拒收
            </View>
            </>
          )
        }
         {
          yishouhuo &&
          (<View className='button-primary' onClick={onSeeSaleData}>
            查看销售数据
          </View>)
        }
         {
          (yishouhuo && !(item.isSaleOut === 1) ) &&
          (<View className='button-primary' onClick={reportTodaySalesData}>
            上报今日销售数据
          </View>)
        }
      </View>
    </View>
  );
}

export default React.memo(Index);
