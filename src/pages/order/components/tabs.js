import React, { useCallback, useEffect, useState } from 'react';
import { Image, View, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
import './tabs.scss';


function Index(props) {
  const { tabList=[], onChange, defaultIndex=0 } = props;
  const [current, setCurrent] = useState(defaultIndex === 'x'? 'x' : parseFloat(defaultIndex));
  // useEffect(()=>{
  //   onChange(parseFloat(defaultIndex));
  // },[defaultIndex]);

  const onClick = (id)=>{
    onChange(id);
    setCurrent(id);
  }

  return (
    <View className='item-tabs'>
      {
        tabList.map((i)=>{
          return (
            <View
              className={i.id === current ? 'item-tabs-label-active' : 'item-tabs-label'}
              onClick={onClick.bind(this,i.id)}
              key={i.id}
            >
              <View className='text'>{i.title}</View>
              <View className='bottom-border' />
            </View>
          )
        })
      }
    </View>
  );
}

export default React.memo(Index);
