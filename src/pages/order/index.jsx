import React, { useCallback, useEffect, useState } from 'react';
import { View } from '@tarojs/components';
import Taro, { usePullDownRefresh, useReachBottom, useRouter } from '@tarojs/taro';
import PageContainer from '../../components/pageContainer';
import ItemCard from './components/itemCard';
import Tabs from './components/tabs';
import events from '../../utils/events';
import './index.scss';
import request from '../../common/request';

const tabList = [
  { title: '全部', id:'x' },
  { title: '待成单', id:0 },
  { title: '待发货', id:1 },
  { title: '待收货', id:2 },
  { title: '已收货', id:3 }
];
const pageSize = 20;
function Index() {
  const router = useRouter();
  const [current, setCurrent] = useState(router.params.status ? parseFloat(router.params.status) : 'x');
  const [list,setList] = useState([]);
  const [ reachBottom, setReachBottom] = useState(false);// 是否可以触底分页  triggered
  // 当前页面
  const [ page, setPage] = useState(1);

  const postData = useCallback((status='', pageIndex=1)=>{
    let orderStatusList = (status === 'x' || status === '' || status === undefined) ? [] : [status];
    if (status === 0) {
      orderStatusList = ['10']
    }
    if (status === 1) {
      orderStatusList = ['20']
    }
    if (status === 2) {
      orderStatusList = ['30','40'];
    }
    if (status === 3) {
      orderStatusList = ['41','42'];
    }
    const parm = {
      condition: { orderStatusList },
      pagingQuery: {
        pageIndex: pageIndex,
        pageSize
      },
    };
    request({method : 'GET', tartget: 'orderStallOrderList', data: parm }).then((data)=>{
      if (data.success) {
        Taro.stopPullDownRefresh();
        if (data.pageInfo.pageIndex*data.pageInfo.pageSize > data.pageInfo.total) {
          setReachBottom(false);
        } else {
          setReachBottom(true);
        }
        if (pageIndex > 1) {
          setPage(pageIndex);
          setList(list.concat(data.models || []));
        } else {
          setPage(pageIndex);
          setList(data.models || []);
        }
      }
    });
  },[]);

  const tabOnChange = useCallback((index)=>{
    setCurrent(index);
    if (index === 0) {
      postData(0);
      return;
    }
    if (index === 1) {
      postData(1);
      return;
    }
    if (index === 2) {
      postData(2);
      return;
    }
    if (index === 3) {
      postData(3);
      return;
    }
    postData();
  },[]);

  const flush = useCallback(() =>{
    postData(current);
  },[current]);

  // 新增编辑完毕后刷新首页列表
  useEffect(()=>{
    events.removeListener('order-page-flush',flush);
    events.on('order-page-flush', flush);
    return () =>{
      events.removeListener('order-page-flush',flush);
    }
  },[flush]);


  // 下拉刷新
  usePullDownRefresh(() => {
    postData(current);
  });

  useReachBottom(()=>{
    if (reachBottom) {
      postData(current, page + 1);
    }
  });

  useEffect(()=>{
    postData(current);
  },[]);

  return (
    <PageContainer noPadding>
      <Tabs tabList={tabList} onChange={tabOnChange} defaultIndex={current} />
      <View className='tabContent'>
        {
          list.length > 0 ?list.map((i)=>{
            return (<ItemCard key={i.id} parms={i} />);
          }): <View style={{ height: '30px', fontSize: '14px', display: 'flex',alignItems: 'center', justifyContent: 'center'}}>暂无数据</View>
        }
      </View>
    </PageContainer>
  )
}

export default Index;
