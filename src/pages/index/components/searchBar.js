import React, { useState } from 'react';
import Taro from '@tarojs/taro'
import { Image, View, Text, Input } from '@tarojs/components';
import VerticalSeparation from '../../../components/VerticalSeparation';
import searchPng from '../../../assets/search.png';
import closePng from '../../../assets/close.png';
import './searchBar.scss';

function Index(props) {
  const [value, setValue] = useState(null);
  const { onClick, disabled, onChange } = props;
  const inputOnChange = (e)=>{
    const inputValue = e.target.value;
    setValue(inputValue);
  }

  const onConfirm = ()=>{
    if (onChange) {
      onChange(value);
    }
  }
  const close = ()=>{
    setValue('');
  }

  if (disabled) {
    return (
      <View className='searchBar-wap-disabled'>
        <VerticalSeparation />
        <View className='searchBar' onClick={onClick}>
          <Image
            className='img'
            src={searchPng}
          />
          <Text className='text'>输入货品名称</Text>
        </View>
        <VerticalSeparation height={22} />
      </View>
    );
  }
  return (
    <View className='searchBarWap'>
      <VerticalSeparation />
      <View className='searchBar-wap'>
        <View className='searchBar' onClick={onClick}>
          <View className='img-wap'>
            <Image
              className='img'
              src={searchPng}
            />
          </View>
          <Input
            value={value}
            className='text'
            type='text'
            onInput={inputOnChange}
            placeholder='输入货品名称'
            placeholderStyle='color:#CCCCCC'
            onConfirm={onConfirm}
          />
          {
            value && (
              <Image
                onClick={close}
                className='close'
                src={closePng}
              />
            )
          }
        </View>
        <View
          className='searchBar-cancel'
          onClick={()=>{
            Taro.navigateBack();
          }}
        >
          取消
        </View>
      </View>
      <VerticalSeparation height={22} />
    </View>
  );
}

export default React.memo(Index);
