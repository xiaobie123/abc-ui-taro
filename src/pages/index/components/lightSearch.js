import React from 'react';
import { View } from '@tarojs/components';

import './lightSearch.scss';

function Index(props) {
  const { } = props;

  return (
    <View className='lightSearch'>
      <View className='text'>
        <View>
          全部市场
        </View>
        <View className='triangle' />
      </View>
      <View className='text'>
        <View>
          全部种类
        </View>
        <View className='triangle' />
      </View>
    </View>
  );
}

export default React.memo(Index);
