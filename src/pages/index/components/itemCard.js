import React, { useCallback } from 'react';
import Taro from '@tarojs/taro';
import { Image, View, Text } from '@tarojs/components';
import { formatTimeDate, getImagePath, navigateTo } from '../../../utils';
import Progress from '../../../components/progress';
import CountDown from '../../../components/countDown';
import './itemCard.scss';

function Index(props) {
  const { item={} } = props;

  const itemCardDetail = useCallback(()=>{
    navigateTo({
      url: `/pages/indexDetail/index?id=${item.id}`
    });
// Taro.navigateTo({
//       url: `/pages/login/canvos`,
//     });
  },[]);

  const path = getImagePath(item.productCoverId,item.productCoverCode);
  return (
    <View>
      <View className='itemCard'>
        {
          item.productCoverId && (
            <Image
              onClick={()=>{
                Taro.previewImage({
                  current: path,
                  urls: [path],
                });
              }}
              className='img'
              src={path}
            />
          )
        }
        <View className='itemCard-right' onClick={itemCardDetail}>
          <View className='text1'>{item.productName}</View>
          <View className='text2'>预计{formatTimeDate(item.expectArriveTime)}到货</View>
          <View className='text3'>
            <View className='text3-left'>
              <View style={{ flex: 1}}>
                <Progress percentage={item.saleQuantity/item.totalQuantity} />
              </View>
              <View className='text'>
                <Text>{item.saleQuantity}</Text>/
                <Text>{item.totalQuantity}</Text>件
              </View>
            </View>
            <View className='text3-right'>
              <CountDown
                noStyle
                time={item.deadline}
                extraTextFun={(is)=>{
                  if (is) {
                    return <Text>截止</Text>
                  } else {
                    return <Text>代卖预订已截止</Text>
                  }
                }}
              />
            </View>
          </View>
          <View className='text4'>
            <View className='text'>￥{item.unitPrice}/件</View>
            <View className='button'>代卖</View>
          </View>
        </View>
      </View>
    </View>
  );
}

export default React.memo(Index);
