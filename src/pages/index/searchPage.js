import React, { useCallback, useState } from 'react';
import Taro from '@tarojs/taro';
import PageContainer from '../../components/pageContainer';
import SearchBar from './components/searchBar';
// import LightSearch from './components/lightSearch';
import ItemCard from './components/itemCard';
import './searchPage.scss';
import request from '../../common/request';
import { View } from '@tarojs/components';

const pageSize = 500;
function Index() {
  const [list,setList] = useState([]);
  const searchBarOnChange = useCallback((value)=>{
    if (!value) {
      Taro.showToast({
        title: '请输入搜索内容',
        icon: 'none',
      });
      return;
    }
    postData(value);
  },[]);

  const postData = useCallback((keyword, pageIndex = 1)=>{
    const parm = {
      condition: {keyword},
      pagingQuery: {
        pageIndex: pageIndex,
        pageSize
      },
    };
    request({method : 'GET', tartget: 'orderConsignorOrderList', data: parm }).then((data)=>{
      if (data.success) {
        setList(data.models || []);
      }
    });
  },[]);

  return (
    <PageContainer noPadding>
      <SearchBar onChange={searchBarOnChange} />
      {/* <LightSearch /> */}
      {
        list.length > 0 ?(
          <>
            {
              list.map((i)=>{
                return (<ItemCard item={i} key={i.id} />);
              })
            }
          </>
        ): (
          <View style={{ height: '30px', fontSize: '14px', display: 'flex',alignItems: 'center', justifyContent: 'center'}}>暂无数据</View>
        )
      }
    </PageContainer>
  );
}

export default Index;
