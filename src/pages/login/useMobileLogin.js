import React from 'react';
import Taro, { useState, useDidShow } from '@tarojs/taro'
import {View, Text, Input} from '@tarojs/components'
// import { AtButton } from 'taro-ui'
import './useMobileLogin.scss'
import { setGlobalData } from '../../common/global_data';
import request from "../../common/request";
import { phoneRegular } from "../../common/regular";

let timeId = null;
function debounce(func, wait) {
  let timer;
  return function() {
    // const context = this; // 注意 this 指向
    let args = arguments; // arguments中存着e
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args)
    }, wait)
  }
}
function UseMobileLogin() {
  const [ time, setTime ] = useState(0);
  const [ item, setItem] = useState( {});

  useDidShow(()=>{
    Taro.hideHomeButton();
  })
  const inputOnChange = (e) =>{
    const name = e.target.dataset.name;
    const value = e.target.value;
    setItem({...item, [name]: value});
  };
  const onClickYZMPre = () => {
    // 校验
    if (!new RegExp(phoneRegular).test(item.phone)) {
      Taro.showToast({
        title: '请填写正确手机号',
        icon: 'none',
      });
      return;
    }
    onClickYZM();
  }
  const onClickYZM = debounce(()=> {
    // 校验
    if (!new RegExp(phoneRegular).test(item.phone)) {
      Taro.showToast({
        title: '请填写正确手机号',
        icon: 'none',
      });
      return;
    }
    request({
      method : 'POST',
      tartget: 'personalValidationCodeSend',
      data: item,
    }).then((data)=>{
      if (data.success) {
        let s = 60;
        setTime(s);
        timeId = setInterval(()=> {
          s = s - 1;
          setTime(s);
          if (s === 0) {
            clearInterval(timeId)
          }
        },1000);
      }
    });
  },500);
  const submit = () => {
    // 校验
    if (!new RegExp(phoneRegular).test(item.phone)) {
      Taro.showToast({
        title: '请填写正确手机号',
        icon: 'none',
      });
      return;
    }
    if (!item.validationCode) {
      Taro.showToast({
        title: '请填写验证码',
        icon: 'none',
      });
      return;
    }
    if (!(item.validationCode.length >= 4 && item.validationCode.length <= 6)) {
      Taro.showToast({
        title: '验证码长度4到6位',
        icon: 'none',
      });
      return;
    }
    request({method : 'POST', tartget: 'personalUserWechatBind', data: {phoneNumber: item.phone, validationCode: item.validationCode } }).then((data)=>{
      if (data.success) {
        Taro.setStorageSync('user', data.model || {});
        setGlobalData('user',data.model);
        Taro.reLaunch({ url: '/pages/index/index'});
      }
    });
  }
  return (
    <View className='phone'>
      <View style={{ padding: '0 28px' }}>
        <View className='title'>
          登录
        </View>
        <View className='list'>
          <View className='item'>
            <Input onInput={inputOnChange} value={item.phone} data-name='phone' className='input' type='text' placeholder='请输入手机号' placeholderStyle='color:#CCCCCC' />
            {
              time !== 0 ?
                (<Text className='extraText' >{`${time}秒后重新获取`}</Text>)
                : (<Text className='extraText' onClick={onClickYZMPre}>获取验证码</Text>)
            }
          </View>
          <View className='item'>
            <Input onInput={inputOnChange} data-name='validationCode' value={item.validationCode} className='input' type='text' placeholder='请输入验证码' placeholderStyle='color:#CCCCCC' />
          </View>
        </View>
        <View className='button' onClick={submit}>
          登录
        </View>
      </View>
    </View>
  )
}

UseMobileLogin.config = {
  navigationBarTitleText: '登录',
}

export default UseMobileLogin;
