import React, { useCallback, useEffect, useState } from 'react';
import Taro, { /*useShareAppMessage, */useDidShow} from '@tarojs/taro'
import { View, Text, Image, Button } from '@tarojs/components'
import request from '../../common/request';
import PageContainer from '../../components/pageContainer';
import defaultHeaderPng from '../../assets/default_header.png';
import loginButtonPng from '../../assets/loginButton.png';
import { formatTimeDate, formatTimeTime  } from '../../utils';
import { saveToken } from '../../common/wx';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import './index.scss'

function Index() {
  const [ refuse, setRefuse] = useState({});
  useDidShow(()=>{
    // Taro.showHomeButton();
  })


  const beginBind = useCallback((e) => {
    if (e.detail.iv) {
      request({method : 'POST', tartget: 'userWechatBind', data: e.detail }).then((data)=>{
        if (data.success) {
          saveToken(data.model);
          Taro.reLaunch({ url: '/pages/index/index'});
        }
      });
    }
  },[]);

  const authen = useCallback(() => {
    Taro.navigateTo({
      url:'/pages/realNameAuthentication/index',
    });
  },[]);

  useEffect(()=>{
    const { certifiedStatus } = Taro.getStorageSync('user') || {};
    if (certifiedStatus === 0 ) {
      request({method : 'GET', tartget: 'personalUserAuthenticationQuery', data: {} }).then((data)=>{
        if (data.success) {
          setRefuse(data.model || {});
        }
      });
    }
  },[]);
  /*
  * export const  CERTIFIEDSTATUS_NOT_REGISTERED = 2; // '未注册'
  export const  CERTIFIEDSTATUS_REGISTERED = 3; // '已注册'
  export const  CERTIFIEDSTATUS_CERTIFYING = 4; // '待认证'
  export const  CERTIFIEDSTATUS_REJECTED = 0; // '未通过'
  export const  CERTIFIEDSTATUS_CERTIFIED = 1; // '已认证'
  *
  * */
  const titleRender = ()=>{
    const { certifiedStatus = 2 } = Taro.getStorageSync('user') || {};
    // // '未注册'
    if (certifiedStatus === 2) {
      return (
        <>
          <View className='textCenter text1'>您还未登录</View>
          <View className='textCenter text2'>请先完成授权并登录</View>
        </>
      );
    }
    // '未通过'
    if ((certifiedStatus === 0)) {
      return (
        <>
          <View className='textCenter text1'>您的认证未通过</View>
          <View className='textCenter text2'>请先重新认证</View>
          <View className='textCenter text2'>驳回时间：{formatTimeDate(refuse.createTime)} {formatTimeTime(refuse.createTime)}</View>
          <View className='textCenter text2'>驳回理由：{refuse.rejectionReason}</View>
        </>
      );
    }
    // 3 '已注册'
    if ((certifiedStatus === 3)) {
      return (
        <>
          <View className='textCenter text1'>您还未认证</View>
          <View className='textCenter text2'>请先完成认证</View>
        </>
      );
    }
    // 4 '待认证'
    if ((certifiedStatus === 4)) {
      return (
        <>
          <View className='textCenter text1'>您已提交认证</View>
          <View className='textCenter text2'>请耐心等候</View>
        </>
      );
    }
    return '';
  }
  const buttonRender = ()=>{
    const { certifiedStatus = 2 } = Taro.getStorageSync('user') || {};
    // // '未注册'
    if (certifiedStatus === 2) {
      return (
        <Button className='button' plain hover-class='none' open-type='getPhoneNumber' onGetPhoneNumber={beginBind}>
          <Image className='loginButton' src={loginButtonPng} />
          <Text className='text'>微信一键登录</Text>
        </Button>
      );
    }
    // 0 '未通过'
    if (certifiedStatus === 0) {
      return (
        <Button className='button' plain hover-class='none' onClick={authen}>
          <Image className='loginButton' src={loginButtonPng} />
          <Text className='text'>认证</Text>
        </Button>
      );
    }
    // 4 '待认证'
    if ((certifiedStatus === 4)) {
      return '';
    }
    // 3 '已注册'
    if ((certifiedStatus === 3)) {
      return (
        <Button className='button' plain hover-class='none' onClick={authen}>
          <Image className='loginButton' src={loginButtonPng} />
          <Text className='text'>认证</Text>
        </Button>
      );
    }
    return '';
  }

  return (
    <PageContainer columnCenter >
      <View className='textCenter headerWap'>
        <Image
          className='header'
          src={defaultHeaderPng}
        />
      </View>
      {
        titleRender()
      }
      <View className='buttonWap3'>
        {
          buttonRender()
        }
      </View>
      {/* <AtButton className='button3' circle type='primary' openType='getPhoneNumber' onGetPhoneNumber={beginBind}>
        获取微信手机号登录
      </AtButton> */}
      {/* <View className='textCenter text4' onClick={yzmLogin}>
        手机验证码注册
      </View> */}
    </PageContainer>
  )
}

export default Index;
