import React, { useEffect } from 'react';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import { checkLogin } from '../../common/wx';

function Loading() {
  useEffect(() => {
    Taro.reLaunch({ url: '/pages/index/index' });
    // 在此校验登录状态
    console.log(checkLogin);
    // checkLogin(true, false);
  }, []);
  return <View />;
}

export default Loading;
