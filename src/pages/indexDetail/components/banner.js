import React from 'react';
import Taro from '@tarojs/taro';
import { Image, View, Swiper, SwiperItem, Text } from '@tarojs/components';
import './banner.scss';
import { getImagePath } from '../../../utils';
import CountDown from '../../../components/countDown';
/*
*
*
* */
function Index(props) {
  const { item={} } = props;
  const fileList = item?.orderProduct?.fileList || [];
  return (
    <View className='banner'>
      <Swiper
        className='banner'
        indicatorColor='#999'
        indicatorActiveColor='#333'
        circular
        indicatorDots
        autoplay
      >
        {
          fileList.map((i)=>{
            return (
              <SwiperItem>
                <Image
                  src={getImagePath(i.fileId,i.fileCode)}
                  className='banner-img'
                  onClick={()=>{
                    Taro.previewImage({
                      current: getImagePath(i.fileId,i.fileCode),
                      urls: fileList.map(it=>getImagePath(it.fileId,it.fileCode)),
                    });
                  }}
                />
              </SwiperItem>
            );
          })
        }
      </Swiper>
      <View className='banner-message'>
        <View className='banner-message-left'>
          <View className='text1'>￥{item.unitPrice}/件</View>
          <View className='text2'>指导价</View>
        </View>
        <View className='banner-message-right'>
          {/* {formatTimeTime(item.deadline)} 后截止 */}
          <CountDown
            time={item.deadline}
            extraTextFun={(is)=>{
              if (is) {
                return <Text>后截止</Text>
              } else {
                return <Text>代卖预订已截止</Text>
              }
            }}
          />
        </View>
      </View>
    </View>
  );
}

export default Index;
