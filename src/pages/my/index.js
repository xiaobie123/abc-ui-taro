import React, { useState, useCallback, useEffect } from 'react';
import Taro,{ useDidShow } from '@tarojs/taro'
import { View, Text, Image } from '@tarojs/components'
// import request from '../../common/request';
import rightPng from '../../assets/right.png';
// import defaultHeaderPng from '../../assets/default_header.png';
import myicon1Png from '../../assets/myicon1.png';
import myicon2Png from '../../assets/myicon2.png';
import myicon3Png from '../../assets/myicon3.png';
import myicon4Png from '../../assets/myicon4.png';
import myicon5Png from '../../assets/myicon5.png';
import myicon6Png from '../../assets/myicon6.png';
import PageContainer from '../../components/pageContainer';
import SetAvatar from '../../components/setAvatar';
import { navigateTo } from '../../utils/index';
// import '../../common/theme.scss'
import './index.scss'
import request from '../../common/request';

const certifiedStatusMap = {
  '0': '未通过',
  '1': '已认证',
  '2': '未注册',
  '3': '已注册',
  '4': '待认证',
};
function Index() {
  const [ item, setItem] = useState({});
  // useDidShow(()=>{
  //   Taro.hideHomeButton();
  // })

  useDidShow(()=>{
    request({method : 'GET', tartget: 'userInfoQuery', data: {} }).then((data)=>{
      if (data.success && data.model) {
        // 存储默认市场
        Taro.setStorageSync('market', {
          marketName:data.model.marketName,
          marketId:data.model.marketId,
        });
        Taro.setStorageSync('user', {
          id: data.model.id,
          name: data.model.name,
          certifiedStatus: data.model.certifiedStatus,
        });
        setItem(data.model || {});
        console.log(data);
      }
    });
  });

  const goOrderPage = (status)=>{
    navigateTo({
      url:`/pages/order/index?status=${status}`,
    });
  }

  // 提示
  useEffect(()=>{
    if (item.certifiedStatus === 0) {
      Taro.showModal({
        title: '提示',
        content: '很抱歉，您所提交的认证申请已被驳回,请检查后重新提交.',
        cancelText:'好的',
        confirmText:'不在提示',
        success: (res) => {
          if (res.confirm) {
            // onDelete();
          }
        },
      });
    }
  },[item.certifiedStatus]);

  const authentication = useCallback(() =>{
    if (parseFloat(item.certifiedStatus) === 4) {
      Taro.showToast({
        title: '你已经提交认证，请耐心等待',
        icon: 'none',
      });
      return;
    }
    if (parseFloat(item.certifiedStatus) === 1) {// 已认证
      navigateTo({
        url:'/pages/realNameAuthentication/detail',
      });
    } else {// 未认证
      navigateTo({
        url:'/pages/realNameAuthentication/index',
      });
    }
  },[item]);
  return (
    <PageContainer noPadding>
      <View className='top'>
        <View className='head-wap'>
          <SetAvatar urlImg={item.avatar}>
            <Image
              className='head'
            />
          </SetAvatar>
        </View>
        <View className='body'>
          <View style={{ display: 'flex', flexDirection:'row', flexWrap: 'wrap'}}>
            <Text className='text1'>{item.stallName || item.nickName || '--'}</Text>
            <Text>{item.phoneNumber || '--'}</Text>
          </View>
          <View onClick={authentication} style={{ display: 'flex', flexDirection:'row', alignItems: 'center'}}>
            <Text className='text2'>批发市场档主</Text>
            <View className='text3'>{certifiedStatusMap[item.certifiedStatus] || '--'}</View>
          </View>
        </View>
      </View>
      <View
        className='title'
        onClick={()=>{
          navigateTo({
            url:`/pages/order/index`,
          });
        }}
      >
        <View>代卖订单</View>
        <Image
          className='titleImg'
          src={rightPng}
        />
      </View>
      <View className='itemWap'>
        <View
          className='itemWap-item'
          onClick={goOrderPage.bind(this,0)}
        >
          <Image
            className='itemWap-item-img myicon1Png'
            src={myicon1Png}
          />
          <View>待成单</View>
        </View>
        <View className='itemWap-item' onClick={goOrderPage.bind(this,1)}>
          <Image
            className='itemWap-item-img myicon2Png'
            src={myicon2Png}
          />
          <View>待发货</View>
        </View>
        <View className='itemWap-item' onClick={goOrderPage.bind(this,2)}>
          <Image
            className='itemWap-item-img myicon3Png'
            src={myicon3Png}
          />
          <View>待收货</View>
        </View>
        <View className='itemWap-item' onClick={goOrderPage.bind(this,3)}>
          <Image
            className='itemWap-item-img myicon4Png'
            src={myicon4Png}
          />
          <View>已收货</View>
        </View>
      </View>
      <View className='division' />
      <View
        className='itemWap'
      >
        <View
          className='itemWap-item'
          onClick={()=>{
            navigateTo({
              url:'/pages/receivingGate/index',
            });
          }}
        >
          <Image
            className='itemWap-item-img myicon5Png'
            src={myicon5Png}
          />
          <View>收货档口</View>
        </View>
        <View className='itemWap-item'>
          <Image
            className='itemWap-item-img myicon6Png'
            src={myicon6Png}
          />
          <View>联系客服</View>
        </View>
        <View className='itemWap-item' />
        <View className='itemWap-item' />
      </View>
    </PageContainer>
  )
}

export default Index;
// {"success":1,"resultCode":"00000","message":"账号绑定成功","model":{"client":"03","sessionKey":"3hBkqMk8vigEOt+fRnK04Q==","operationTime":1591683129468,"token":"0127580b249e8cfbe1bdd2824d8e8012","userId":342,"account":"1231231","userName":"别","phone":"15554723096","isEnable":1,"isAdmin":0,"customerNo":"CUS201806270091","customerName":"上海崇固实业有限公司","isDownloadDoc":1}}
