import React, { useCallback, useEffect, useRef, useState } from 'react';
import { View } from '@tarojs/components'
import Taro, { useRouter } from '@tarojs/taro';
// import request from '../../common/request';
import PageContainer from '../../components/pageContainer';
import VerticalSeparation from '../../components/VerticalSeparation';
import FormList from '../../components/formList';
import PagefooterButton from '../../components/pagefooterButton';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import events from '../../utils/events';
import './edit.scss'
import request from '../../common/request';

const initValues = { isDefault: 0};
function Index() {
  const market = Taro.getStorageSync('market');
  const formRef = useRef(null);
  const router = useRouter();
  const [values,setValues] = useState(initValues);
  // const [marketIdList, setMarketIdList] = useState({});
  // useDidShow(()=>{
  //   Taro.hideHomeButton();
  // })

  useEffect(()=>{
    // 编辑需要查询详情
    if (router.params.type!=='add') {
      request({method : 'GET', tartget: 'userStallUQuery', data: {condition: {id:router.params.id}} }).then((data)=>{
        if (data.success) {
          setValues(data.model);
        }
      });
    }
    // request({method : 'GET', tartget: 'baseMarketList', data: {} }).then((data)=>{
    //   if (data.success) {
    //     setMarketIdList(data.models);
    //   }
    // });
  },[router.params]);

  const onDelete = useCallback(()=>{
    request({method : 'POST', tartget: 'userStallDelete', data: [values] }).then((data)=>{
      if (data.success) {
        events.emit('receivingGate-page-flush',values);
        Taro.navigateBack();
      }
    });
  },[values]);

  const onFinse = useCallback(()=>{
    formRef.current.validateFields((parms,error)=>{
      if (!error) {
        const url = values.id ? 'userStallUpdate' : 'userStallAdd';
        request({method : 'POST', tartget: url, data: [{...parms, marketId: market.marketId }] }).then((data)=>{
          if (data.success) {
            events.emit('receivingGate-page-flush',parms);
            Taro.navigateBack();
          }
        });
        console.log(parms);
      }
    })
  },[formRef, values]);

  return (
    <View>
      <PageContainer noPadding>
        <VerticalSeparation />
        <FormList formRef={formRef} initValues={values}>
          <FormList.InputItem disabled={values.isCertified === 1} type='inputText' label='收货人' name='receiverName' require />
          <FormList.InputItem disabled={values.isCertified === 1} type='inputText' label='手机号码' name='receiverPhoneNumber' require />
          {/* <FormList.InputItem
            disabled={values.isCertified === 1}
            range={marketIdList}
            type='inputSelect'
            label='所属市场'
            name='marketId'
            rangeKey='marketName'
            require
          /> */}
          <FormList.ItemText label='所属市场' value={market.marketName} />
          <FormList.InputItem disabled={values.isCertified === 1} type='inputText' label='档口编号' name='stallCode' require />
          <FormList.InputItem type='inputSwitch' label='设置默认收货档口' name='isDefault' />
        </FormList>
      </PageContainer>
      {
        !values.id || values.isCertified === 1 ? (
          <PagefooterButton
            oneButton={{
              text:'保存',
              onClick:onFinse,
            }}
          />
        ):(
          <PagefooterButton
            leftButton={{
              text:'删除',
              onClick:()=>{
                Taro.showModal({
                  title: '确认删除',
                  content: '确定要删除吗?',
                  success: (res) => {
                    if (res.confirm) {
                      onDelete();
                    }
                  },
                });
              },
            }}
            rightButton={{
              text:'保存',
              onClick:onFinse,
            }}
          />
        )
      }
    </View>
  )
}

export default Index;
