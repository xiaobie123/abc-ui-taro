import React, { useState, useCallback, useEffect } from 'react';
import { Image, View } from '@tarojs/components';
// import request from '../../common/request';
import Taro, { usePullDownRefresh } from '@tarojs/taro';
import PageContainer from '../../components/pageContainer';
import VerticalSeparation from '../../components/VerticalSeparation';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import './index.scss'
import events from '../../utils/events';
import addPng from '../../assets/add.png';
import editPng from '../../assets/edit.png';
import request from '../../common/request';

function Index() {
  const [list,setList] = useState([]);

  // 查询列表接口
  const postQueryList = useCallback(()=>{
    request({method : 'GET', tartget: 'userStallList', data: {} }).then((data)=>{
      if (data.success) {
        Taro.stopPullDownRefresh();
        setList(data.models);
      }
    });
  },[]);

  // 新增编辑完毕后刷新首页列表
  useEffect(()=>{
    events.on('receivingGate-page-flush', function () {
      postQueryList();
    })
  },[]);

  // 默认查询
  useEffect(()=>{
    postQueryList();
  },[]);

  // 下拉刷新
  usePullDownRefresh(() => {
    postQueryList();
  });

  return (
    <PageContainer>
      <VerticalSeparation />
      {
        list.map((i)=>{
          return (
            <View key={i.id}>
              <View
                className='item'
                onClick={()=>{
                  Taro.navigateTo({
                    url: `/pages/receivingGate/edit?id=${i.id}`,
                  });
                }}
              >
                <View className='item-top'>
                  <View className='item-top-left'>
                    <View className='text1'>{i.receiverName}</View>
                    <View className='text2'>{i.receiverPhoneNumber}</View>
                    {
                      i.isCertified ===1 && <View className='text3'>认证</View>
                    }
                    {
                      i.isDefault === 1 && <View className='text4'>默认</View>
                    }
                  </View>
                  <Image
                    className='item-top-edit'
                    src={editPng}
                  />
                </View>
                <View className='item-down'>
                  <View>{i.marketName}/{i.stallCode}档口</View>
                </View>
              </View>
              <VerticalSeparation height={20} />
            </View>
          );
        })
      }
      <Image
        onClick={()=>{
          Taro.navigateTo({
            url:'/pages/receivingGate/edit?type=add',
          });
        }}
        className='addButtonImg'
        src={addPng}
      />
    </PageContainer>
  )
}

export default Index;
