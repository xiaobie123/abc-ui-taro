import React from 'react';
import { View, Image, Text } from '@tarojs/components';
import './success.scss';
import successPng from '../../assets/success.png';
import Button from '../../components/button';
import PageContainer from '../../components/pageContainer';

function Index() {
  return (
    <PageContainer>
      <View className='successPage'>
        <Image className='img' src={successPng} />
        <Text className='text1'>订单提交成功</Text>
        <Text className='text2'>请耐心等待货主处理发货</Text>
        <Button>查看订单详情</Button>
      </View>
    </PageContainer>
  );
}

export default Index;
