import React, { useCallback, useEffect, useState } from 'react';
import Taro from '@tarojs/taro'
import { Image, View, Text } from '@tarojs/components';
import './selectStall.scss';
import editPng from '../../../assets/edit.png';
import VerticalSeparation from '../../../components/VerticalSeparation';
import request from '../../../common/request';
import events from '../../../utils/events';
import Button from '../../../components/button';


function Index(props){
  const { onChange } = props;
  const [list,setList] = useState([]);

  // 查询列表接口
  const postQueryList = useCallback(()=>{
    request({method : 'GET', tartget: 'userStallList', data: {} }).then((data)=>{
      if (data.success) {
        setList(data.models);
      }
    });
  },[]);

  // 新增编辑完毕后刷新首页列表
  useEffect(()=>{
    events.on('receivingGate-page-flush', function () {
      postQueryList();
    })
  },[]);

  // 默认查询
  useEffect(()=>{
    postQueryList();
  },[]);

  const selectStall = (item) =>{
    if (onChange) {
      onChange(item);
    }
  }

  return (
    <View className='selectStall'>
      {
        list.map((i)=>{
          return (
            <View>
              <VerticalSeparation height={20} />
              <View className='item'>
                <View className='item-body' onClick={selectStall.bind(this,i)}>
                  <View className='item-top'>
                    <View>
                      {i.marketName}|{i.stallCode}档口
                    </View>
                  </View>
                  <View className='item-down'>
                    <Text className='text1'>{i.receiverName}</Text>
                    <Text className='text2'>{i.receiverPhoneNumber}</Text>
                    {
                      i.isCertified ===1 && <Text className='text3'>认证</Text>
                    }
                    {
                      i.isDefault === 1 && <Text className='text4'>默认</Text>
                    }
                  </View>
                </View>
                {/* <View
                  className='item-edit'
                  onClick={()=>{
                    Taro.navigateTo({
                      url: `/pages/receivingGate/edit?id=${i.id}`,
                    });
                  }}
                >
                  <Image
                    className='img'
                    src={editPng}
                  />
                </View> */}
              </View>
            </View>
          );
        })
      }
      <View className='button'>
        {/* <Button
          onClick={()=>{
            Taro.navigateTo({
              url: `/pages/receivingGate/edit?type=add`,
            });
          }}
        >
          新增
        </Button> */}
      </View>
    </View>
  )
}

export default Index;

