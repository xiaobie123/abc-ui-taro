import React, { useCallback } from 'react';
import Taro from '@tarojs/taro'
import { Input, View } from '@tarojs/components';
import './index.scss';


function Index(props){
  const { value=0, onChange } = props;

  const inputOnChange = useCallback((e)=>{
    if (onChange) {
      const inputValue = e.target.value;
      onChange(inputValue);
    }
  },[onChange]);

  return (
    <View className='inputNumber'>
      <Input
        value={value}
        className='input'
        type='digit'
        onInput={inputOnChange}
        // placeholder='请选择'
        placeholderStyle='color:#CCCCCC'
      />
    </View>
  )
}

export default Index;

