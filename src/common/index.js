import api from './api';
import config from './config';
import global_data from './global_data';
import request from './request';
import wx from './wx';

export default {
  api,
  config,
  globalData: global_data,
  request,
  wx,
}

