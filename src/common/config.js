// 存放系统配置
// 军哥
// const origin = 'http://192.168.2.120:12300';
// const fileOrigin = 'http://192.168.2.120:12800';
// 乔木
// const origin = 'http://192.168.2.252:12300';
// const fileOrigin = 'http://192.168.2.120:12800';

// sit
const origin = 'https://www.vichain.com';
const fileOrigin = 'https://www.vichain.com';

// 存放系统配置

// const origin = 'http://192.168.2.100';
// const fileOrigin = 'http://192.168.2.100';

const baseApi = '/green-weskit-market/api';
const fileApi = '/green-weskit-file/api';

export default {
  origin,
  apiPrefix: origin + baseApi, // 接口地址
  filePrefix: fileOrigin + fileApi, // 文件接口地址
  client: '04', // 客户端标识
  primaryColor: '#44b549',
};
