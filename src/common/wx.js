import Taro from '@tarojs/taro';
import request from './request';

export function getSystemInfo() {
  // 获取系统信息
  Taro.getSystemInfo({
    success: res => {
      var modelmes = res.model;
      let isIphoneX = 0;
      if (
        modelmes.search('iPhone X') > -1 ||
        modelmes.search('unknown') > -1 ||
        modelmes.search('iPhone12') > -1
      ) {
        isIphoneX = 1;
      }
      // 缓存到本地
      try {
        Taro.setStorage({ key: 'isIphoneX', data: isIphoneX });
        Taro.setStorage({ key: 'windowHeight', data: res.windowHeight });
        Taro.setStorage({ key: 'windowWidth', data: res.windowWidth });
        Taro.setStorage({ key: 'pixelRatio', data: res.pixelRatio });
      } catch (error) {
        console.log(error);
      }
    },
  });
}

// 微信获取code
export function wxLogin() {
  console.log(2);
  // 登录
  Taro.login({
    async success(res) {
      console.log(res);
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      const result = await request({
        method: 'POST',
        tartget: 'userLogin',
        data: { code: res.code },
      });
      console.log(result);
      // const result = { success: 1, token: 'test1234' };
      if (result.success && result.model) {
        console.log(3);
        // todo保存用户信息
        saveToken(result.model);
        // 跳转到首页
        // if (result.model.userId) {
        //   isNeedReLaunch && Taro.reLaunch({ url: '/pages/index/index' });
        //   // getUserWxInfo();
        //   return;
        // } else {
        //   // 跳转到登录页面
        //   isNeedLogin && Taro.reLaunch({ url: '/pages/login/index' });
        // }
        Taro.reLaunch({ url: '/pages/index/index' });
      }

      // if (result.success === 0) {
      //   // 跳转到登录页面
      //   isNeedLogin && Taro.reLaunch({ url: '/pages/login/index' });
      // }
    },
  });
}
export async function checkLogin(isNeedReLaunch, isNeedLogin) {
  // 判断是否登录(需要根据是否需要登录)
  // 本地token
  var token = Taro.getStorageSync('token');
  // var user = Taro.getStorageSync("user") || {};
  if (!token) {
    wxLogin(isNeedReLaunch, isNeedLogin);
    return;
  }
  // 根据token获取用户信息
  const result = await request({ method: 'POST', tartget: 'userLogin', data: {} });
  // token有效
  if (result.success && result.model) {
    // todo保存用户信息
    saveToken(result.model);
    // 跳转到首页
    // if (result.model.userId) {
    //   // getUserWxInfo();
    //   Taro.reLaunch({ url: '/pages/index/index' });
    //   return;
    // }
    // 跳转到登录页面
    Taro.reLaunch({ url: '/pages/index/index' });
  }
  // token已无效 过期
  if (result.resultCode === '00999') {
    wxLogin(isNeedReLaunch, isNeedLogin);
  }
}

export const saveToken = (model = {}) => {
  // todo保存用户信息
  if (model.token){
    Taro.setStorageSync('token', model.token);
  }
  Taro.setStorageSync('user', {
    id: model.userId,
    name: model.userName,
    certifiedStatus: model.certifiedStatus,
  });
};
const requestPreCheckWinXinLogin = () => {
  return new Promise(function(resolve) {
    // 微信登录
    Taro.login({
      async success(res) {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        const result = await request({
          method: 'POST',
          tartget: 'personalUserLogin',
          data: { code: res.code },
          check: true,
        });
        if (result.success && result.model && result.model.userId) {
          // todo保存用户信息
          saveToken(result.model);
          resolve(1);
        } else {
          resolve(0);
          Taro.reLaunch({ url: '/pages/login/authorizedMobile' });
        }
      },
    });
  });
};
// 1 成功   2 失败（跳到登录页）  (用于在请求之前确保token有效)
export async function requestPreCheckLogin() {
  // 本地token
  var token = Taro.getStorageSync('token');
  // 要走微信登录
  if (!token) {
    const a = await requestPreCheckWinXinLogin();
    return a;
  } else {
    const result = await request({
      method: 'POST',
      tartget: 'personalUserLogin',
      data: {},
      check: true,
    });
    if (result.success && result.model && result.model.userId) {
      // todo保存用户信息
      saveToken(result.model);
      return 1;
    } else if (result.resultCode === '00999') {
      // taken 过期
      // 要走微信登录
      const a = await requestPreCheckWinXinLogin();
      return a;
    } else {
      Taro.reLaunch({ url: '/pages/login/authorizedMobile' });
      return 0;
    }
  }
}

export function getUserWxInfo() {
  Taro.getSetting({
    success: res => {
      if (res.authSetting['scope.userInfo']) {
        // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
        Taro.getUserInfo({
          success: ({ userInfo = {} }) => {
            Taro.setStorageSync('avatarUrl', userInfo.avatarUrl);
            // request({ tartget: 'updateUserAvatar', data: { avatar: userInfo.avatarUrl } });
          },
        });
      }
    },
  });
}
