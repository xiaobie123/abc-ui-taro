// 存放常量
// 用户认证状态
export const  CERTIFIEDSTATUS_NOT_REGISTERED = 2; // '未注册'
export const  CERTIFIEDSTATUS_REGISTERED = 3; // '已注册'
export const  CERTIFIEDSTATUS_CERTIFYING = 4; // '待认证'
export const  CERTIFIEDSTATUS_REJECTED = 0; // '未通过'
export const  CERTIFIEDSTATUS_CERTIFIED = 1; // '已认证'
