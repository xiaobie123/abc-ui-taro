import React, { Component }  from 'react';
import Taro from '@tarojs/taro';
import {View, Canvas, Text} from '@tarojs/components';

let ctx = Taro.createCanvasContext('canvas', this);
let startX = 0;
let startY = 0;
let canvasw = 0;
let canvash = 0;


export default class canvos extends Component {
  state = {
    isPaint: false,// 是否已经签名
    isShow: true, // 是否展示文字说明
  }

  initCanvas() {
    ctx = Taro.createCanvasContext('canvas', this);
    ctx.setStrokeStyle('#000000');
    ctx.setLineWidth(4);
    ctx.setLineCap('round');
    ctx.setLineJoin('round');
  }

  canvasStart = (e) => {
    if (startX !== 0) {
      this.setState({
        isShow: false
      })
    }
    startX = e.changedTouches[0].x;
    startY = e.changedTouches[0].y;
    ctx.beginPath()
  }

  canvasMove = (e) => {
    if (startX !== 0) {
      this.setState({
        isPaint: true,
        isShow: false
      })
    }
    let x = e.changedTouches[0].x
    let y = e.changedTouches[0].y
    ctx.moveTo(startX, startY)
    ctx.lineTo(x, y)
    ctx.stroke();
    ctx.draw(true)
    startX = x
    startY = y
  }

  canvasEnd(e) {
    console.log('结束')
  }

  // 重新签名
  clearDraw = () => {
    startX = 0;
    startY = 0;
    ctx.clearRect(0, 0, canvasw, canvash);
    ctx.draw(true);
    this.setBackground();
    this.setState({
      isPaint: false,
    })
  }

  // 设置背景色
  setBackground = () =>{
    // 背景色
    ctx.fillStyle= '#fff';
    ctx.fillRect(0, 0, canvasw, canvash);
    ctx.draw(true)
  }

  // 生成图片
  createImg(fun) {
    if (!this.state.isPaint) {
      Taro.showToast({
        title: '签名内容不能为空！',
        icon: 'none'
      });
      return false;
    }
    // 生成图片
    Taro.canvasToTempFilePath({
      canvasId: 'canvas',
      success: async res => {
        fun(res);
      },
      fail(err) {
        console.log(err)
      }
    })
  }

  // 获取 canvas 的尺寸（宽高）
  getCanvasSize() {
    const query = Taro.createSelectorQuery()
    query.select('#canvas').boundingClientRect(function (res) {
      canvasw = res.width;
      canvash = res.height;

      // 背景色
      ctx.fillStyle= '#fff';
      ctx.fillRect(0, 0, canvasw, canvash);
      ctx.draw(true);
    })
    query.exec()
  }

  componentDidMount() {
    setTimeout(()=>{
      this.getCanvasSize();
    },500);
    setTimeout(()=>{

    });
    this.initCanvas()
  }

  componentWillUnmount() {
    ctx = null
  }

  handleHideWarp = () => {
    this.setState({
      isShow: false
    })
  }

  render() {
    return (
      <View className='canvas-box'>
        <Canvas
          id='canvas'
          canvasId='canvas'
          className='canvas'
          disableScroll
          onTouchStart={this.canvasStart.bind(this)}
          onTouchMove={this.canvasMove.bind(this)}
          onTouchEnd={this.canvasEnd.bind(this)}
          onTouchCancel={this.canvasEnd.bind(this)}
          onClick={this.handleHideWarp}
          disable-scroll
        >
        </Canvas>
        {
          this.state.isShow &&
          <View className='text_warp' onTouchStart={this.handleHideWarp} onClick={this.handleHideWarp}>
            <Text className='text-detail' >请在此处签名</Text>
          </View>
        }
      </View>
    );
  }
}
