import Group from './group';
import ImagePreview from './imagePreview';

const ImagePreviewTrans = ImagePreview;
ImagePreviewTrans.Group = Group;
export { Group };
export default ImagePreviewTrans;
