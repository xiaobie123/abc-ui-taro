import React from 'react';
import { View } from '@tarojs/components';
import './index.scss';
import ImagePreview from '../index';

function Index() {
  return (
    <View className='imgPage'>
      <View className='item'>
        <View className='title'>单个图片预览</View>
        <ImagePreview
          className='img'
          src='https://storage.360buyimg.com/mtd/home/111543234387022.jpg'
        />
        <ImagePreview
          className='img'
          src='https://storage.360buyimg.com/mtd/home/221543234387016.jpg'
        />
      </View>
      <View className='item'>
        <View className='title'>多个图片预览</View>
        <ImagePreview.Group>
          <ImagePreview
            className='img'
            src='https://storage.360buyimg.com/mtd/home/111543234387022.jpg'
          />
          <ImagePreview
            className='img'
            src='https://storage.360buyimg.com/mtd/home/221543234387016.jpg'
          />
        </ImagePreview.Group>
      </View>
    </View>
  );
}

export default Index;
