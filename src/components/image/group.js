import React, { useState, useEffect } from 'react';

export const context = React.createContext({
  isPreviewGroup: false,
  previewUrls: [],
  setPreviewUrls: () => null,
});

/**
 @name 多图片预览
 * */
const { Provider } = context;

function Group(props) {
  const [previewUrls, setPreviewUrls] = useState([]);

  useEffect(() => {}, []);

  return (
    <Provider
      value={{
        isPreviewGroup: true,
        previewUrls,
        setPreviewUrls,
      }}
    >
      {props.children}
    </Provider>
  );
}
Group.defaultProps = {};

Group.propTypes = {};
export default Group;
