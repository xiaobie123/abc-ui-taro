import React from 'react';
import { View } from '@tarojs/components'
import './pageContainer.scss';

/*
* @一个通用外壳组件
*
* */
function Index(props){
  let className = 'columnLeft';
  // 预设一个布局： column center
  if (props.columnCenter) {
    className = 'columnCenter';
  }
  if (props.noPadding) {
    className = 'noPadding';
  }
  return (
    <View className={className}>
      {props.children}
    </View>
  )
}

export default Index;

