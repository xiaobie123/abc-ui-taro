import React, { useCallback, useEffect, useState } from 'react';
// import Taro, { usePullDownRefresh, } from '@tarojs/taro';
import { View, Text } from '@tarojs/components';
import FloatLayout from '../index';

 const  Demo = ()=> {
  const [isOpened, setIsOpened] = useState(false);

  const handleClose = ()=>{
    setIsOpened(false);
  }

  return (
    <View>
      <Text onClick={()=>{setIsOpened(true)}}>打开模态框</Text>
      <FloatLayout isOpened={isOpened} title='这是个标题' onClose={handleClose}>
        这是内容区 随你怎么写这是内容区 随你怎么写这是内容区 随你怎么写这是内容区
        随你怎么写这是内容区 随你怎么写这是内容区 随你怎么写
      </FloatLayout>
    </View>
  );
}
export default Demo;
