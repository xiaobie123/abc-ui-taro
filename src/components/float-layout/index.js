import React, { useCallback, useEffect, useState } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { ScrollView, Text, View } from '@tarojs/components';
import './float-layout.scss';

const FloatLayout = (props)=>{
  const {
    title,
    scrollY,
    scrollX,
    scrollTop,
    scrollLeft,
    upperThreshold,
    lowerThreshold,
    scrollWithAnimation,
    isOpened,
  } = props

  const rootClass = classNames(
    'at-float-layout',
    {
      'at-float-layout--active': isOpened
    },
    props.className
  )
  return (
    <View className={rootClass}>
      <View onClick={props.onClose} className='at-float-layout__overlay' />
      <View className='at-float-layout__container layout'>
        {title ? (
          <View className='layout-header'>
            <Text className='layout-header__title'>{title}</Text>
            <View className='layout-header__btn-close' onClick={props.onClose} />
          </View>
        ) : null}
        <View className='layout-body'>
          <ScrollView
            scrollY={scrollY}
            scrollX={scrollX}
            scrollTop={scrollTop}
            scrollLeft={scrollLeft}
            upperThreshold={upperThreshold}
            lowerThreshold={lowerThreshold}
            scrollWithAnimation={scrollWithAnimation}
            // 预留给后面需要用滚动条做点什么的时候
            onScroll={props.onScroll}
            onScrollToLower={props.onScrollToLower}
            onScrollToUpper={props.onScrollToUpper}
            className='layout-body__content'
          >
            {props.children}
          </ScrollView>
        </View>
      </View>
    </View>
  )
}

FloatLayout.defaultProps = {
  title: '',
  isOpened: false,
  scrollY: true,
  scrollX: false,
  scrollWithAnimation: false,
  onClose: () => {},
  onScroll: () => {},
  onScrollToLower: () => {},
  onScrollToUpper: () => {}
}

FloatLayout.propTypes = {
  title: PropTypes.string,
  isOpened: PropTypes.bool,
  scrollY: PropTypes.bool,
  scrollX: PropTypes.bool,
  scrollTop: PropTypes.number,
  scrollLeft: PropTypes.number,
  upperThreshold: PropTypes.number,
  lowerThreshold: PropTypes.number,
  scrollWithAnimation: PropTypes.bool,
  onClose: PropTypes.func,
  onScroll: PropTypes.func,
  onScrollToLower: PropTypes.func,
  onScrollToUpper: PropTypes.func
}
export default FloatLayout;
