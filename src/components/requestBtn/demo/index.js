import React, { useState } from 'react';
import { View } from '@tarojs/components';
import RequestBtn from '../index';
import request from '../../../common/request';

export default function RequestBtnIndex() {
  const [checked, setChecked] = useState(false);
  const [loading, setLoading] = useState(false);

  const onSubmit = () => {
    console.log('发出请求（提交请求延迟2s');
    setLoading(RequestBtn.loading);

    wx.showToast({ title: '发出请求', icon: 'none' }); // eslint-disable-line

    setTimeout(() => {
      request({
        method: 'POST',
        tartget: 'userLogin',
        data: { code: '1-029876' },
      });
    }, 2000);
  };

  const onChecked = () => {
    return checked;
  };

  const onClick = () => {
    if (checked) {
      setChecked(false);
    } else {
      setChecked(true);
    }
  };

  const onReset = function() {
    RequestBtn.reset();
    setLoading(RequestBtn.loading);
  };

  const onGetLoading = function() {
    setLoading(RequestBtn.loading);
  };

  return (
    <View style={{ padding: '10vw' }}>
      <View style={{ marginBottom: '2vh' }}>内部loading ({loading ? '禁止' : '允许'}) </View>

      <RequestBtn style={{ color: 'blue' }} onSubmit={onSubmit} onChecked={onChecked}>
        点击请求数据（{checked ? '外部响应' : '外部不响应'}）（
        {RequestBtn.loading ? '内部禁止' : '内部允许'}）
      </RequestBtn>

      <View style={{ marginTop: '2vh', color: 'red' }} onClick={onClick}>
        点击改变外部权限 ({checked ? '外部响应' : '外部不响应'}）
      </View>

      <View style={{ marginTop: '2vh', color: 'orange' }} onClick={onReset}>
        点击重置内部loading状态
      </View>

      <View style={{ marginTop: '2vh', color: 'pink' }} onClick={onGetLoading}>
        点击获取内部loading状态
      </View>
    </View>
  );
}
