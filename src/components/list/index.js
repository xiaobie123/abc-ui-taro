import React, { useCallback, useEffect, useRef, useState } from 'react';
import Taro, { usePullDownRefresh, useReachBottom } from '@tarojs/taro';
import { AtList } from 'taro-ui';
import 'taro-ui/dist/style/components/list.scss';
import 'taro-ui/dist/style/components/icon.scss';
import './index.scss';
import request from '../../common/request';

const PAGE_SIZE = 30;

function List({
  target, // 目标api
  itemRender, // item渲染组件
  hasBorder = true, // list是否需要border
  initCondition = {}, // 初始化参数
  isInitFetch = true, // 初始化是否需要请求数据
  defaultValue = [], // 初始化默认值
  pageSize = PAGE_SIZE, // 每页数量
  listRef, // ref
}) {
  const [, setLoading] = useState(false); // 记录页面loading状态(用于页面更新状态)
  const [list, setList] = useState(defaultValue);
  const isFetch = useRef(false); // 记录请求状态(用于记录请求数据状态)
  const hashMore = useRef(true);
  const pageIndex = useRef(1);
  const currCondition = useRef(initCondition);
  const currPageSize = useRef(pageSize);

  // 请求列表数据
  const getList = useCallback(
    async (params = {}) => {
      isFetch.current = true;
      setLoading(true);
      const result = await request({
        target,
        data: params,
      });
      if (result.success) {
        hashMore.current =
          params.pagingQuery.pageIndex * params.pagingQuery.pageSize < result.pageInfo.total;
        pageIndex.current = params.pagingQuery.pageIndex;
        setList(pre => (params.condition.cursor ? [...pre, ...result.models] : result.models));
      }
      Taro.stopPullDownRefresh();
      setLoading(false);
      isFetch.current = false;
    },
    [target]
  );
  // 初始化
  useEffect(() => {
    if (isInitFetch) {
      getList({
        condition: currCondition.current,
        pagingQuery: {
          pageIndex: pageIndex.current,
          pageSize: currPageSize.current,
        },
      });
    }
  }, [getList, isInitFetch]);

  // 下拉刷新
  usePullDownRefresh(() => {
    hashMore.current = true;
    pageIndex.current = 1;
    getList({
      condition: currCondition.current,
      pagingQuery: {
        pageIndex: pageIndex.current,
        pageSize: currPageSize.current,
      },
    });
  }, []);
  // 上拉加载
  useReachBottom(() => {
    if (hashMore.current) {
      getList({
        condition: currCondition.current,
        pagingQuery: {
          pageIndex: pageIndex.current,
          pageSize: currPageSize.current,
        },
      });
    }
  }, []);
  // 手动搜索数据
  const search = useCallback(
    params => {
      if (isFetch.current) {
        return;
      }
      hashMore.current = true;
      pageIndex.current = 1;
      currCondition.current = params;
      getList({
        condition: currCondition.current,
        pagingQuery: {
          pageIndex: pageIndex.current,
          pageSize: currPageSize.current,
        },
      });
    },
    [getList]
  );
  // 更新item
  const updateItem = useCallback((data, key = 'id') => {
    setList(pre => {
      return pre.map(item => {
        if ((item[key] = data[key])) {
          return data;
        }
        return item;
      });
    });
  }, []);
  // 删除item
  const delItem = useCallback((value, key = 'id') => {
    setList(pre => {
      return pre.filter(item => item[key] !== value);
    });
  }, []);

  // 开放接口
  useEffect(() => {
    if (listRef && typeof formRef !== 'function') {
      listRef.current.search = search; // 手动搜索
      listRef.current.updateItem = updateItem; // 更新item
      listRef.current.delItem = delItem; // 删除item
      listRef.current.getList = () => list; // 获取列表数据
      listRef.current.setList = setList; // 设置列表数据
    }
  }, [delItem, list, listRef, search, updateItem]);
  return (
    <AtList hasBorder={hasBorder}>
      {list.map((item, index) => {
        return itemRender(item, index);
      })}
    </AtList>
  );
}

export default List;
