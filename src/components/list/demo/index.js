import React, { useRef, useCallback, useState } from 'react';
import { AtListItem, AtSearchBar } from 'taro-ui';
import 'taro-ui/dist/style/components/search-bar.scss';
import 'taro-ui/dist/style/components/button.scss';
import 'taro-ui/dist/style/components/icon.scss';
import List from '../index';
import './index.scss';

function Index() {
  const listRef = useRef({});
  const [keyword, setKeyword] = useState('');

  const itemReader = (item, index) => {
    // 自定义获取使用默认样式如下
    return (
      <AtListItem title='标题文字' extraText='详细信息' onClick={false} arrow='right' key={index} />
    );
  };
  const onSearch = useCallback(() => {
    listRef.current.search({ keyword });
  }, [keyword]);
  return (
    <>
      <AtSearchBar value={keyword} onChange={value => setKeyword(value)} onActionClick={onSearch} />
      <List
        itemRender={itemReader}
        hasBorder={false}
        initCondition={{ keyword }}
        defaultValue={[{}, {}, {}]}
        listRef={listRef}
      />
    </>
  );
}

export default Index;
