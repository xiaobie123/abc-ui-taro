import React from 'react';
import { View } from '@tarojs/components'
import './index.scss';
import VerticalSeparation from '../VerticalSeparation';

/*
* @一个通用底部按钮
* @两种按钮
* */
function Index(props){
  const {
    leftButton={},
    rightButton={},
    oneButton={},
    component,// 组件全部来自外部
    className,
  } = props;

  // 底部只有一个按钮
  if (oneButton.text) {
    return (
      <View className='footerOneButtonWap'>
        {
          oneButton.addonBefore && oneButton.addonBefore
        }
        <View className={`oneButton ${oneButton.disabled && 'disabled'}`} onClick={oneButton.onClick}>{oneButton.text}</View>
      </View>
    )
  }
  // 底部有2个按钮
  return (
    <View>
      <VerticalSeparation height={68} />
      <View className='footerButtonWap'>
        {
          component ? <View className={className}>{component}</View> : (
            <>
              <View className='button button1' onClick={leftButton.onClick}>
                {
                  leftButton.textRender ? leftButton.textRender: leftButton.text
                }
              </View>
              <View className='button button2' onClick={rightButton.onClick}>{rightButton.text}</View>
            </>
          )
        }
      </View>
    </View>
  )
}

export default Index;

