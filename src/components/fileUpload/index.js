import React, { useCallback, useEffect, useState } from 'react';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import request from '../../common/request';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import closeCirclePng from '../../assets/closeCircle.png';
import imageDefaultPng from '../../assets/imageDefault.png';
import uploadImage from '../../common/upload';
import { getImagePath } from '../../utils/index';
import './index.scss'

// 追加path: 因为编辑的时候需要使用下载接口下载文件
const addPath = (list) =>{
  const listTem = list;
  listTem.map((i)=>{
    const iTem = i;
    if (!i.path && i.fileId && i.fileCode) {
      const url = getImagePath(i.fileId,i.fileCode);
      iTem.path = url;
      return iTem
    }
    return iTem;
  });
  return listTem;
};

function Index(props) {
  const { label, list, detail } = props;
  const [defaultList, setDefaultList] = useState([]);
  // const [selfieFile, setSelfieFile] = useState([]);
  // useDidShow(()=>{
  //   Taro.hideHomeButton();
  // })

  // init
  useEffect(()=>{
    setDefaultList(list || []);
  },[list]);

  const onChangeSelfieFile = useCallback(() => {
    uploadImage({
      list: defaultList || [],
      maxNum: 1,
      callback: data => {
        setDefaultList([...data]);
      },
    });
  }, [defaultList, setDefaultList]);
  // 删除
  const onFilterSelfieFile = useCallback(path => {
    let listTem = defaultList;

    listTem = listTem.filter(item => {
      item.path !== path;
    });

    setDefaultList(listTem);
  }, [defaultList]);

  const onPreviewImage = useCallback((fileList, path) => {
    Taro.previewImage({
      current: path,
      urls: fileList.map(item => getImagePath(item.fileId, item.fileCode)),
    });
  }, []);


  // 放开对外控制
  useEffect(() => {
    const action = {
      getFileList: ()=>{
        return defaultList;
      },
    };
    const { actionRef } = props;
    if (actionRef && typeof actionRef !== 'function') {
      actionRef.current = action;
    }
  }, [defaultList]);
  const selfieFile = addPath(defaultList || []);
  return (
    <View className='selfieFileWrap'>
      <View className='title'>
        {
          require && (
            <Text className='itemRequire'>*</Text>
          )
        }
        {label}
      </View>
      <View className='imageList'>
        {selfieFile.map(item => {
          return (
            <View key={item.path} className='imageWrap'>
              <Image src={item.path} onClick={onPreviewImage.bind(this,selfieFile,item.path)} className='imageContent' />
              {
                !detail && (
                  <Image
                    src={closeCirclePng}
                    className='imageColse'
                    onClick={onFilterSelfieFile.bind(this,item.path)}
                  />
                )
              }
            </View>
          );
        })}
        <Image src={imageDefaultPng} className='imageContent' onClick={onChangeSelfieFile} />
      </View>
    </View>
  )
}

export default React.memo(Index);
// {"success":1,"resultCode":"00000","message":"账号绑定成功","model":{"client":"03","sessionKey":"3hBkqMk8vigEOt+fRnK04Q==","operationTime":1591683129468,"token":"0127580b249e8cfbe1bdd2824d8e8012","userId":342,"account":"1231231","userName":"别","phone":"15554723096","isEnable":1,"isAdmin":0,"customerNo":"CUS201806270091","customerName":"上海崇固实业有限公司","isDownloadDoc":1}}
