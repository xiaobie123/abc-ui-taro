import React, { useEffect, useRef, useState } from 'react';
import { View, Text } from '@tarojs/components';
import './index.scss';
import { formatNumber, formatTimeDate } from '../../utils/index';
/*
*
*
* */
// 计算倒计时
const hour72 = 24 * 60 * 60 * 1000*3;
export function countdownTime(time, mode) {
  let diff = time - new Date().getTime();
  if (!diff || diff < 0) {
    return;
  }
  let baseTime = 24 * 60 * 60 * 1000; // 天基数
  const day = Math.floor(diff / baseTime);
  diff = diff % baseTime;
  baseTime = baseTime / 24;
  let hour = Math.floor(diff / baseTime);
  diff = diff % baseTime;
  baseTime = baseTime / 60;
  const minute = Math.floor(diff / baseTime);
  diff = diff % baseTime;
  baseTime = baseTime / 60;
  const seconds = Math.floor(diff / baseTime);

  if (mode == 1) {
    hour += day * 24;
    return [hour, minute, seconds]
      .map(formatNumber)
      .join(':')
      .split('');
  }
  return `${day}天${hour}时${minute}分`;
}
function Index(props) {
  const [date, setDate] = useState([]);
  const refObj = useRef(null);
  const { time, extraTextFun, noStyle } = props;
  let diff = time - new Date().getTime();

  useEffect(()=>{
    setDate(countdownTime(time, 1) || []);
    if (time && (time - new Date().getTime()) < hour72) {
      refObj.current = setInterval(()=>{
        setDate(countdownTime(time, 1) || []);
      },1000);
    }
    return ()=>{
      clearInterval(refObj.current);
    }
  },[time]);
  // 大于72小时不显示倒计时
  if (diff >hour72) {
    return (
      <View className='countDown-noStyle'>
        <Text className='number'>
          {formatTimeDate(time)}
        </Text>
        截止
      </View>
    );
  }

  if (noStyle) {
    return (
      <View className='countDown-noStyle'>
        <Text className='number'>
          {date}
        </Text>
        {
          extraTextFun && extraTextFun(countdownTime(time, 1))
        }
      </View>
    );
  }
  return (
    <View className='countDown'>
      {
        date.map((i)=>{
          if (i === ':') {
            return <View className='fenGe'>:</View>
          } else {
            return <View className='number'>{i}</View>
          }
        })
      }
      {
        extraTextFun && extraTextFun(countdownTime(time, 1))
      }
    </View>
  );
}

export default Index;
