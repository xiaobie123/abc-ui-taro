import React from 'react';
import { Image, View, Text } from '@tarojs/components';
import './index.scss';
/*
*
* @进度条，首页详情页适用
*
* */
function Index(props) {
  const { percentage } = props;
  return (
    <View className='progress'>
      <View className='progressItem' style={{ width: `${percentage*100 || 0}%` }}>
        {`${ (percentage*100).toFixed(0) || 0}%`}
      </View>
    </View>
  );
}

export default Index;
