import React  from 'react';
import { View } from '@tarojs/components'
import './index.scss';


function Index(props){
  const { onClick, children } = props;
  return (
    <View className='button-com' onClick={onClick}>{children}</View>
  )
}

export default React.memo(Index);

