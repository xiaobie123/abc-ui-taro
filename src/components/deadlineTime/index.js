import React, { useCallback, useState } from 'react';

/*
* @ deadlineTime
*
* */
function Index(){
  const [value,setValue] = useState('');
  const deadlineTime = useCallback(time => {
    let diff = time - new Date().getTime();
    if (!diff || diff < 0) {
      return `0天0时0分`;
    }
    let baseTime = 24 * 60 * 60 * 1000; //天
    const day = Math.floor(diff / baseTime);
    diff = diff % baseTime;
    baseTime = baseTime / 24;
    const hour = Math.floor(diff / baseTime);
    diff = diff % baseTime;
    baseTime = baseTime / 60;
    const minute = Math.floor(diff / baseTime);

    setValue(`${day}天${hour}时${minute}分`);
  }, []);

  return {
    setTime:deadlineTime,
    time:value,
  }
}

export default React.memo(Index);

