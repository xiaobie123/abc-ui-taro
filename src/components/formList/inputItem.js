import React, { useCallback, useContext, useEffect, useState } from 'react';
import { View, Text, Image, Input, Picker } from '@tarojs/components'
import { AtSwitch } from 'taro-ui'
// import request from '../../common/request';
import rightPng from '../../assets/right.png';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import FormContext from './context';
import './inputItem.scss'
/*
* 预设类型如下
*
* inputText
* inputNumber
* inputSelect
  inputSwitch
* ...
* */
const arr = [];
function Index(props) {
  const [index,setIndex] = useState(null);
  const { name, label, disabled, require, type='inputText', rangeKey='name' } = props;
  const { range=arr } = props;
  const { values={}, setValue, pushRequireArr, detail } = useContext(FormContext);

  // 保存必填校验字段
  useEffect(()=>{
    if (require) {
      // setTimeout((()=>{
      //   pushRequireArr(name);
      // },1))
      pushRequireArr({name,label});
    }
  },[require, name, label, pushRequireArr]);

  // 默认 select   index
  useEffect(()=>{
    if (type === 'inputSelect') {
      const rangeTEm = range || [];
      for (let i = 0; i < rangeTEm.length; i++) {
        if (rangeTEm[i].id === values[name]) {
          setIndex(i);
          return;
        }
      }
      setIndex(null);
    }
  },[name, range, type, values]);

  // input
  const inputOnChange = useCallback((e) =>{
    const inputName = e.target.dataset.name;
    const inputValue = e.target.value;
    setValue(inputName, inputValue);
  },[setValue]);

  // select
  const inputSelectOnChange = useCallback((e) =>{
    const indexTem = e.detail.value;
    setIndex(indexTem);
    setValue(name, range[indexTem].id);
  },[setValue, name, range]);

  // Switch
  const inputSwitchOnChange = useCallback((e) =>{
    const inputName = name;
    const inputValue = e;
    setValue(inputName, inputValue ? 1:0);
  },[setValue, name]);

  // if (detail) {
  //   return (
  //     <View className='form-List-input-item'>
  //       <View className='itemLeft'>
  //         <Text className='itemLeft-label'>{label}</Text>
  //       </View>
  //       <View className='itemRight'>
  //         <Input
  //           disabled
  //           value={values[name]}
  //           className='itemRight-input'
  //           type='text'
  //           // placeholder='请选择'
  //           placeholderStyle='color:#CCCCCC'
  //         />
  //         <Image
  //           className='itemRight-input-right'
  //           src={rightPng}
  //         />
  //       </View>
  //     </View>
  //   )
  // }
  const disabledTem= !!(detail || disabled);
  if (type === 'inputText') {
    return (
      <View className='form-List-input-item'>
        <View className='itemLeft'>
          {
            require && (
              <Text className='itemRequire'>*</Text>
            )
          }
          <Text className='itemLeft-label'>{label}</Text>
        </View>
        <View className='itemRight'>
          <Input
            disabled={disabledTem}
            onInput={inputOnChange}
            value={values[name]}
            data-name={name}
            className='itemRight-input'
            type='text'
            placeholder='请输入'
            placeholderStyle='color:#CCCCCC'
          />
        </View>
      </View>
    )
  }
  if (type === 'inputSelect') {
    return (
      <Picker disabled={disabledTem} rangeKey={rangeKey} mode='selector' range={range} onChange={inputSelectOnChange}>
        <View className='form-List-input-item'>
          <View className='itemLeft'>
            {
              require && (
                <Text className='itemRequire'>*</Text>
              )
            }
            <Text className='itemLeft-label'>{label}</Text>
          </View>
          <View className='itemRight'>
            <Input
              disabled
              value={range[index]&& range[index][rangeKey]}
              className='itemRight-input'
              type='text'
              placeholder='请选择'
              placeholderStyle='color:#CCCCCC'
            />
            {
              !disabledTem && (
                <Image
                  className='itemRight-input-right'
                  src={rightPng}
                />
              )
            }
          </View>
        </View>
      </Picker>
    )
  }
  if (type === 'inputSwitch') {
    return (
      <View className='form-List-input-item'>
        <View className='itemLeft'>
          {
            require && (
              <Text className='itemRequire'>*</Text>
            )
          }
          <Text className='itemLeft-label'>{label}</Text>
        </View>
        <View className='itemRight'>
        <AtSwitch color='#44b549' checked={values[name]} onChange={inputSwitchOnChange} />
        </View>
      </View>
    )
  }
  return <View>无</View>
}

export default React.memo(Index);
