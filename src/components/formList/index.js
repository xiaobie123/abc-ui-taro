import React, { useState, useEffect, useCallback, useRef } from 'react';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import FormContext from './context';
import inputItem from './inputItem';
import fileItem from './fileItem';
import ItemText from './itemText';
import RegionSelectItem from './regionSelectItem';
import './index.scss';
/**
 * 表单组件
 * @param {*} props
 */
function Index(props) {
  const requireArr = useRef([]); // 保存 必填校验字段名称   【{name:'price',label: '单价'}】
  const [values, setValues] = useState({}); // 保存 数据
  // const [ requireArr, setRequireArr] = useState([]);// 保存 必填校验字段名称   【‘key1’，'key2'】
  const { initValues, detail } = props;

  // 初始化设置value
  useEffect(() => {
    setValues(initValues || {});
  }, [initValues]);

  /**
   * @param key
   * @param value
   * **/
  const setValue = useCallback(
    (key, value) => {
      setValues({
        ...values,
        [key]: value,
      });
    },
    [values]
  );

  // 保存必填项
  const pushRequireArr = useCallback(key => {
    if (!requireArr.current.length) {
      requireArr.current = [];
    }
    requireArr.current.push(key);
  }, []);

  // 获取 校验后的
  const getFieldsValue = useCallback(() => {
    return values;
  }, [values]);

  // 设置表单的值
  // todo    设置的值有可能会不生效，  子节点也有存  values，一块更新的时候，可能会被覆盖    因为子节点也可可能在操作这个值 哎
  // 在change的时候 和这个在一块使用的时候，要注意一定要 同步操作，不然顺序很难保证   详见  regionSelectItem.js 里面的change
  //
  const setFieldsValue = useCallback((obj) => {
    setValues({...values,...obj});
  }, [values]);

  // 表单校验
  const validateFields = useCallback(
    fun => {
      let error = false;
      // 必填校验
      for (let i = 0; i < requireArr.current.length; i++) {
        if (!values[requireArr.current[i].name] && values[requireArr.current[i].name] !== 0) {
          Taro.showToast({
            title: `[${requireArr.current[i].label}]字段不能为空`,
            icon: 'none',
          });
          error = true;
          fun(values, error);
          return;
        }
      }
      return fun(values, error);
    },
    [values, requireArr]
  );

  // 放开对外控制
  useEffect(() => {
    const action = {
      getFieldsValue,
      validateFields,
      setFieldsValue,
    };
    const { formRef } = props;
    if (formRef && typeof formRef !== 'function') {
      formRef.current = action;
    }
  }, [getFieldsValue, getFieldsValue, setFieldsValue]);

  return (
    <FormContext.Provider value={{ setValue, values, pushRequireArr, detail }}>
      <View className='wap'>{props.children}</View>
    </FormContext.Provider>
  );
}
Index.InputItem = inputItem;
Index.FileItem = fileItem;
Index.ItemText = ItemText;
Index.RegionSelectItem = RegionSelectItem;
export default Index;
