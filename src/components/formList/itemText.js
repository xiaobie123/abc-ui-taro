import React from 'react';
import { View, Text, Input, Image } from '@tarojs/components';
// import request from '../../common/request';
// import rightPng from '../../assets/right.png';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
// import FormContext from './context';
import './inputItem.scss'
import rightPng from '../../assets/right.png';
/*
* 预设类型如下
*
* inputText
* inputNumber
* inputSelect
  inputSwitch
* ...
* */
function Index(props) {
  const { label } = props;
  const { value, valueRender } = props;
  return (
    <View className='form-List-input-item'>
      <View className='itemLeft'>
        <Text className='itemLeft-label'>{label}</Text>
      </View>
      <View className='itemRight'>
        {valueRender ? valueRender : <View className='itemRight-input'>{value}</View>}
        {/* <Image
          className='itemRight-input-right'
          src={rightPng}
        /> */}
      </View>
    </View>
  )
}

export default React.memo(Index);
// {"success":1,"resultCode":"00000","message":"账号绑定成功","model":{"client":"03","sessionKey":"3hBkqMk8vigEOt+fRnK04Q==","operationTime":1591683129468,"token":"0127580b249e8cfbe1bdd2824d8e8012","userId":342,"account":"1231231","userName":"别","phone":"15554723096","isEnable":1,"isAdmin":0,"customerNo":"CUS201806270091","customerName":"上海崇固实业有限公司","isDownloadDoc":1}}
