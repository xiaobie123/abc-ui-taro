import React, { useCallback, useContext, useEffect } from 'react';
import { View, Image, Text } from '@tarojs/components';
import Taro from '@tarojs/taro';
// import request from '../../common/request';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import FormContext from './context';
import './fileItem.scss';
import closeCirclePng from '../../assets/closeCircle.png';
import imageDefaultPng from '../../assets/imageDefault.png';
import wenhaoPng from '../../assets/wenhao.png';
import uploadImage from '../../common/upload';
import { getImagePath } from '../../utils/index';
/*
* 预设类型如下
*
* inputText
* inputNumber
* inputSelect
  inputSwitch
* ...
* */

// 追加path: 因为编辑的时候需要使用下载接口下载文件
const addPath = list => {
  const listTem = list;
  listTem.map(i => {
    const iTem = i;
    if (!i.path && i.fileId && i.fileCode) {
      const url = getImagePath(i.fileId, i.fileCode);
      // debugger;
      iTem.path = url;
      return iTem;
    }
    return iTem;
  });
  return listTem;
};

function Index(props) {
  // const [selfieFile, setSelfieFile] = useState([]);
  // useDidShow(()=>{
  //   Taro.hideHomeButton();
  // })
  const { name, label, multiple, multipleLength, require, describe } = props;
  const { values = {}, setValue, pushRequireArr, detail } = useContext(FormContext);

  const onChangeSelfieFile = useCallback(() => {
    uploadImage({
      list: values[name] || [],
      maxNum: 1,
      callback: data => {
        // debugger;
        // console.log(data);
        // setSelfieFile(data);
        setValue(name, data);
      },
    });
  }, [setValue]);

  // 删除
  const onFilterSelfieFile = useCallback(
    path => {
      let list = values[name];
      list = list.filter(item => item.path !== path);
      setValue(name, list);
    },
    [values]
  );

  // 点击弹出描述信息
  const onClickDescribe = useCallback(
    () => {
      Taro.showToast({
        title: describe,
        icon: 'none',
      });
    },
    [describe]
  );

  const onPreviewImage = useCallback((list, path) => {
    Taro.previewImage({
      current: path,
      urls: list.map(item => getImagePath(item.fileId, item.fileCode)),
    });
  }, []);

  // 保存必填校验字段
  useEffect(() => {
    if (require) {
      pushRequireArr({name,label});
    }
  }, [require, name, label]);
  const selfieFile = addPath(values[name] || []);
  return (
    <View className='selfieFileWrap'>
      <View className='title'>
        {
          require && (
            <Text className='itemRequire'>*</Text>
          )
        }
        {label}
        {
          describe && (
            <Image onClick={onClickDescribe} src={wenhaoPng} className='describe-img' />
          )
        }
      </View>
      <View className='imageList'>
        {selfieFile.map(item => {
          return (
            <View key={item.path} className='imageWrap'>
              <Image onClick={onPreviewImage.bind(this,selfieFile,item.path)} src={item.path} className='imageContent' />
              {!detail && (
                <Image
                  src={closeCirclePng}
                  className='imageColse'
                  onClick={onFilterSelfieFile.bind(this,item.path)}
                />
              )}
            </View>
          );
        })}
        {
          multipleLength ? (
            <>
              {(selfieFile.length < multipleLength) && (
                <Image src={imageDefaultPng} className='imageContent' onClick={onChangeSelfieFile} />
              )}
            </>
          ):(
            <>
              {(selfieFile.length === 0 || multiple) && (
                <Image src={imageDefaultPng} className='imageContent' onClick={onChangeSelfieFile} />
              )}
            </>
          )
        }
      </View>
    </View>
  );
}

export default React.memo(Index);
