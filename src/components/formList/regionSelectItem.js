import React, { useCallback, useContext, useEffect } from 'react';
import { View, Text, Image, Input, Picker } from '@tarojs/components'
// import request from '../../common/request';
import rightPng from '../../assets/right.png';
// import { login } from '../../common/wx';
// import '../../common/theme.scss'
import FormContext from './context';
import './inputItem.scss'
/*
* 国家地区选择
*
*
* */
function Index(props) {
  const { name, label, disabled, require } = props;
  const { onChange } = props;
  const { values={}, setValue, pushRequireArr, detail } = useContext(FormContext);

  // 保存必填校验字段
  useEffect(()=>{
    if (require) {
      pushRequireArr({name,label});
    }
  },[require, name, label]);

  // select
  const inputSelectOnChange = useCallback((e) =>{
    const indexTem = e.detail.value;
    setValue(name, indexTem);
    setTimeout(()=>{
      if (onChange) {
        onChange(indexTem);
      }
    },1);
  },[setValue, name, onChange]);

  // const regionOnChange = useCallback(e => {
  //   const value = e.detail.value;
  //   state.goods.region = value;
  //   state.goods.marketRegion = value[0];
  //   state.goods.marketCity = value[1];
  //   state.goods.marketArea = value[2];
  //   // 切换档口
  //   if (value.join(' ') !== state.goods.regionText) {
  //     state.goods.market = null;
  //     state.goods.marketId = null;
  //     state.goods.marketName = null;
  //     getMarketList({
  //       condition: {
  //         marketRegion: value[0],
  //         marketCity: value[1],
  //         marketArea: value[2],
  //       },
  //     });
  //   }
  //   state.goods.regionText = value.join(' ');
  //   setUpdate();
  //   // eslint-disable-next-line
  // }, []);

  const disabledTem= !!(detail || disabled);
  return (
    <Picker
      disabled={disabledTem}
      mode='region'
      data-name='region'
      // value={[goods.marketRegion, goods.marketCity, goods.marketArea]}
      value={values[name] || []}
      onChange={inputSelectOnChange}
    >
      <View className='form-List-input-item'>
        <View className='itemLeft'>
          {
            require && (
              <Text className='itemRequire'>*</Text>
            )
          }
          <Text className='itemLeft-label'>{label}</Text>
        </View>
        <View className='itemRight'>
          <Input
            disabled
            value={(values[name] || []).join('')}
            className='itemRight-input'
            type='text'
            placeholder='请选择'
            placeholderStyle='color:#CCCCCC'
          />
          {
            !disabledTem && (
              <Image
                className='itemRight-input-right'
                src={rightPng}
              />
            )
          }
        </View>
      </View>
    </Picker>
  )
}

export default React.memo(Index);
