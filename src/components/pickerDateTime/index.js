import React, { useEffect, useState } from 'react';
import { Picker } from '@tarojs/components';

//
export default function PickerDateTime({ children, mode, defaultValue, onChange }) {
  const [pickerList, setPickerList] = useState([]);
  const [value, setValue] = useState([]);

  // 获取某一个月到天数
  const getMonthDate = (year, month) => {
    var d = new Date(year, month, 0);
    return d.getDate();
  };

  useEffect(() => {
    // 初始化基础数据
    const date = new Date();
    const y = date.getFullYear();
    const m = date.getMonth();
    const len = getMonthDate(y, m + 1);
    const year = Array.from({ length: 100 }).map((item, i) => 1970 + i + '年');
    const month = Array.from({ length: 12 }).map((item, i) => `00${1 + i}`.substr(-2) + '月');
    const day = Array.from({ length: len }).map((item, i) => `00${1 + i}`.substr(-2) + '日');
    const hour = Array.from({ length: 24 }).map((item, i) => `00${i}`.substr(-2) + '时');
    let minute = '';
    const listTemp = [year, month, day, hour];
    if (mode !== 'hour') {
      minute = Array.from({ length: 4 }).map((item, i) => `00${15 * i}`.substr(-2) + '分');
      listTemp.push(minute);
    }

    setPickerList(listTemp);
  }, [mode]);
  useEffect(() => {
    // 初始化值
    const date = defaultValue ? new Date(defaultValue) : new Date();
    const y = date.getFullYear();
    const m = date.getMonth();
    const d = date.getDate();
    const h = date.getHours();
    const mi = date.getMinutes();
    const newValue = [y - 1970, m, d - 1, h];
    if (mode !== 'hour') {
      const temp = { 0: 0, 15: 1, 30: 2, 45: 3 };
      newValue.push(temp[mi] || 0);
    }
    setValue(newValue);
  }, [mode, defaultValue]);

  const onColumnChange = e => {
    const valueTemp = [...value];
    const pickerListTemp = [...pickerList];
    const column = e.detail.column;
    const index = e.detail.value;
    valueTemp[column] = index;
    // 年份和月份会影响其他列到数据
    if (column === 0 || column === 1) {
      const year = pickerListTemp[0][valueTemp[0]];
      const month = pickerListTemp[1][valueTemp[1]];
      const d = getMonthDate(Number.parseInt(year), Number.parseInt(month)); //天数
      // 滑动月份天数不一样，就
      if (pickerListTemp[2].length !== d) {
        pickerListTemp[2] = Array.from({ length: d }).map(
          (item, i) => `00${1 + i}`.substr(-2) + '日'
        );
        setPickerList(pickerListTemp);
      }
      if (valueTemp[2] >= d) {
        valueTemp[2] = 0;
      }
    }
    setValue(valueTemp);
  };

  const onOk = e => {
    const tempValue = e.detail.value;
    const before = pickerList.map((item, i) => item[tempValue[i]].replace(/年|月|日|时|分/, ''));
    const after = before.splice(mode === 'hour' ? -1 : -2);
    while (after.length < 3) {
      after.push('00');
    }
    const picker = [before.join('-'), after.join(':')];
    const pickerText = picker.join(' ');
    const time = new Date(pickerText.replace(/-/g, '/')).getTime();
    onChange && onChange(time);
  };
  return (
    <Picker
      value={value}
      mode='multiSelector'
      range={pickerList}
      onChange={onOk}
      onColumnChange={onColumnChange}
    >
      {children}
    </Picker>
  );
}
