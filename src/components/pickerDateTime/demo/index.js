import React, { useState } from 'react';
import { Text, View } from '@tarojs/components';
import { useDidShow } from '@tarojs/taro';
import PickerDateTime from '../index';
import './index.scss';
import { countdownTime, formatDateTime } from '../../../utils';

function PickerDateTimeDemo() {
  const [dateTime, setDateTime] = useState('');
  const [lastDateTime, setLastDateTime] = useState(null);
  useDidShow(() => {
    const temp = '2021-01-01 00:00:00';
    setLastDateTime(countdownTime(new Date(temp).getTime(), 1));
    setInterval(() => {
      setLastDateTime(countdownTime(new Date(temp).getTime(), 1));
    }, 1000);
  }, []);
  return (
    <View>
      <PickerDateTime onChange={data => setDateTime(data)}>
        <View className='row'>
          <View className='title'>请选择日期时间</View>
          <Text className='detail'>{dateTime ? formatDateTime(dateTime, '-') : '请选择'}</Text>
        </View>
      </PickerDateTime>
      <View className='row'>
        <View className='title'>距离2021-01-01</View>
        {lastDateTime && (
          <Text className='detail'>
            {`${lastDateTime.day}天${lastDateTime.hour}时${lastDateTime.minute}分${lastDateTime.seconds}秒` ||
              '请重新传时间'}
          </Text>
        )}
      </View>
    </View>
  );
}

export default PickerDateTimeDemo;
