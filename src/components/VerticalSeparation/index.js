import React from 'react';
import { View } from '@tarojs/components'
import './index.scss';

/*
* @一个通用垂直间隔
*
* */
function Index(props){
  let className = 'height16';
  if(props.height === 20) {
    className = 'height20';
  }
  if(props.height === 22) {
    className = 'height22';
  }
  if(props.height === 22) {
    className = 'height22';
  }
  if(props.height === 58) {
    className = 'height58';
  }
  if(props.height === 68) {
    className = 'height68';
  }
  if (props.topSeparaton) {
    className = 'topSeparaton';
  }
  return (
    <View className={className} />
  )
}

export default Index;

