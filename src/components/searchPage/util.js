import Taro from '@tarojs/taro';

/**
 * @description 通过唯一id保存搜索历史
 * */
export const saveKeyWordCache = (pageUniqueId,value)=>{
  let list = getKeyWordCache(pageUniqueId) || [];
  /**
  * @剔除重复
  * */
  if (list.indexOf(value)>-1){
    list.splice(list.indexOf(value),1);
  }
  /**
   * @添加到头部
   * */
  list = [value,...list];
  /**
   * @保存
   * */
  Taro.setStorageSync(`${pageUniqueId}-KeyWordCache`, list);
  return ''
}
/**
 * @description 通过唯一id获取本地存储的搜索历史
 * */
export const getKeyWordCache = (pageUniqueId)=>{
  return Taro.getStorageSync(`${pageUniqueId}-KeyWordCache`) || [];
}
/**
 * @description 通过唯一id获取本地存储的搜索历史
 * */
export const deleteKeyWordCache = (pageUniqueId)=>{
  Taro.setStorageSync(`${pageUniqueId}-KeyWordCache`, []);
}
