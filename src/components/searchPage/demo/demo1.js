import React from 'react';
// import Taro from '@tarojs/taro';
import { View } from '@tarojs/components'
import SearchPage from '../index';
import './demo1.scss';


function Index() {
  return (
    <View className='searchPage'>
      <SearchPage
        pageUniqueId='demo1'
        request={({ condition, pagingQuery }) =>{
          const params = {
            condition,
            pagingQuery,
          };
          return new Promise((resolve)=>{
            setTimeout(()=>{
              console.log(params);
              resolve({
                success:1,
                models:[{name:'ni'},{name:'wo'}],
              });
            },1000);
          })
        }}
        renderItem={(record)=>{
          return (
            <View className='item'>{record.name}</View>
          )
        }}
      />
    </View>
  )
}

export default Index;
