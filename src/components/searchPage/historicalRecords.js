import React, { useCallback, useState } from 'react';
import { View } from '@tarojs/components';
import { AtIcon, AtTag } from 'taro-ui'
import { getKeyWordCache,deleteKeyWordCache } from './util';
/**
 * @description 显示历史记录
 * */
function HistoricalRecords(props) {
  /**
  * @强制刷新组件
  * */
  const [, reFlush] = useState(null);

  const { pageUniqueId, tagOnClick } = props;

  /**
  * @删除历史记录
  * */
  const atTagOnDelete = useCallback(() => {
    deleteKeyWordCache(pageUniqueId);
    reFlush(new Date().getTime());
  }, [pageUniqueId]);

  const list = getKeyWordCache(pageUniqueId);

  return (
    <View className='historicalRecords'>
      <View className='at-row title'>
        <View className='at-col'>历史搜索</View>
        <View className='at-col text-right'>
          <AtIcon onClick={atTagOnDelete} value='trash' size='18'></AtIcon>
        </View>
      </View>
      <View>
        {
          list.map((i) => {
            return <AtTag onClick={tagOnClick.bind(null,i)}>{i}</AtTag>;
          })
        }
      </View>
      <View style={{
        height: '30px',
        fontSize: '14px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
      >暂无数据</View>
    </View>
  );
}

export default HistoricalRecords;
