import React from 'react';
import { View } from '@tarojs/components'
import './index.scss';

/*
* @一个通用card组件
*
* */
const pre = 'cardContainer-';
function Index(props){
  let className = `${pre}columnLeft`;
  // 预设一个布局： column center
  if (props.columnCenter) {
    className = `${pre}columnCenter`;
  }
  if (props.noPadding) {
    className = `${pre}noPadding`;
  }
  return (
    <View className={className}>
      {props.children}
    </View>
  )
}

export default React.memo(Index);

