import React, { useCallback, useEffect, useState } from 'react';
import { AtModal } from "taro-ui";


function Index(props){
  const [ isOpened, setIsOpened ] = useState(false);
  const show = useCallback(()=>{setIsOpened(true) },[]);
  const hide = useCallback(()=>{setIsOpened(false) },[]);
  useEffect(()=>{
    if (props.actionRef) {
      props.actionRef.current = {
        show,
        hide,
      }
    }
  },[]);

  return (
    <AtModal
      isOpened={isOpened}
      {
        ...props
      }
      onClose={hide}
      onCancel={hide}
    />
  )
}

export default React.memo(Index);
