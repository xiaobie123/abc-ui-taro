import React, { useCallback, useEffect, useRef, useState } from 'react';
// import Taro from '@tarojs/taro';
import { Picker, View } from '@tarojs/components'

import {
  AtInput, AtButton, AtInputNumber, AtRadio ,AtCheckbox, AtRate, AtSwitch,
  AtList, AtListItem,AtImagePicker
}  from 'taro-ui'
import Form, { Item } from '../../../components/form';
import './demo1.scss';

const initValues = { isDefault: 0};
const checkboxOption = [{
  value: 'list1',
  label: 'iPhone X',
  desc: '部分地区提供电子普通发票，用户可自行打印，效力等同纸质普通发票，具体以实际出具的发票类型为准。'
},{
  value: 'list2',
  label: 'HUAWEI P20'
}]
const rangeCity = ['美国', '中国', '巴西', '日本'];
function Index() {
  const formRef = useRef(null);
  const [values,setValues] = useState(initValues);

  useEffect(()=>{
    setValues({remark: 'xxxxxx'});
  },[]);

  const onFinish = useCallback(()=>{
    formRef.current.validateFields((parms,success)=>{
      if (success) {
        console.log(parms,success);
      }
    })
  },[formRef, values]);

  return (
    <Form className='formList' formRef={formRef} initValues={values}>
      <Item name='name' label='姓名' rules={[{type:'require'}]}>
        <AtInput
          title='姓名'
          type='text'
          placeholder='请输入'
        />
      </Item>
      <Item
        name='age'
        label='年龄'
        rules={
          [
            {type:'require'},
            {validate:()=>{}}
          ]
        }
      >
        <AtInput
          title='年龄'
          type='text'
          placeholder='请输入'
        />
      </Item>
      <Item label='手机号' name='phone' rules={[{type:'require'},{type:'phone'}]}>
        <AtInput
          title='手机号'
          type='number'
          placeholder='请输入'
        />
      </Item>
      {/* 自定义 */}
      <Item name='city' normalize={(e)=>e.detail.value}>
        {
          ({ value, onChange})=>{
            return (
              <Picker value={value} mode='selector' range={rangeCity} onChange={onChange}>
                <AtList>
                  <AtListItem
                    title='国家地区'
                    extraText={rangeCity[value]}
                  />
                </AtList>
              </Picker>
            );
          }
        }
      </Item>
      <View>
        <Item name='number1'>
          <AtInputNumber
            min={0}
            max={10}
            step={1}
          />
        </Item>
      </View>
      <Item name='radio' valuePropEventName='onClick'>
        <AtRadio
          options={[
            { label: '单选项一', value: 'option1', desc: '单选项描述' },
            { label: '单选项二', value: 'option2' },
          ]}
        />
      </Item>
      <Item name='checkbox' valuePropName='selectedList'>
        <AtCheckbox
          options={checkboxOption}
        />
      </Item>
      <Item name='Rate'>
        <AtRate />
      </Item>
      <Item name='AtSwitch' valuePropName='checked'>
        <AtSwitch title='是否开启' />
      </Item>
      <Item
        name='files'
        valuePropName='files'
        normalize={(e)=>{
          /*
          * todo 这里做些掉接口传文件的事情
          * */
          return e;
        }}
      >
        <AtImagePicker
          // files={this.state.files}
          // onChange={this.onChange.bind(this)}
        />
      </Item>
      <AtButton onClick={onFinish} type='primary'>提交</AtButton>
    </Form>
  )
}

export default Index;
