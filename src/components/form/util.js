import Taro from '@tarojs/taro';

const phoneRegular = '^1[3456789]\\d{9}$';
/**
* @name 校验方法
* @requireArr
* @values
* */
export const checkValue = (requireArr, values)=>{
  let success = false;
  for (let i = 0; i < requireArr.length; i++) {
    const currentValue = values[requireArr[i].name];
    const currentRules = requireArr[i].rules;
    const currentLabel = requireArr[i].label;
    for (let j = 0; j < currentRules.length; j++) {
      /**
       * @非空
       * */
      if (currentRules[j].type === 'require' && !currentValue){
        Taro.showToast({
          title: `[${currentLabel}]字段不能为空`,
          icon: 'none',
        });
        return success;
      }
      /**
       * @手机号
       * */
      if (currentRules[j].type === 'phone' && !new RegExp(phoneRegular).test(currentValue)){
        Taro.showToast({
          title: '请填写正确手机号',
          icon: 'none',
        });
        return success;
      }
      /**
       * @扩展
      *  todo 各种扩展，可以在这里
      * */
    }
  }
  success = true;
  return success;
}
