import Form from './form';
import Item from './item';
import './index.scss';

const FormTrans = Form;
FormTrans.Item = Item;
export { Item };
export default FormTrans;
