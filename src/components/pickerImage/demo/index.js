import React, { useCallback, useState } from 'react';
import PickerImage from '../index';
import './index.scss';

function PickerImageDemo() {
  const [files, setFiles] = useState([
    {
      url: 'https://storage.360buyimg.com/mtd/home/111543234387022.jpg',
    },
  ]);
  const onChangeFiles = useCallback(data => setFiles(data), []);
  return <PickerImage files={files} multiple count={9} length={3} onChangeFiles={onChangeFiles} />;
}

export default PickerImageDemo;
